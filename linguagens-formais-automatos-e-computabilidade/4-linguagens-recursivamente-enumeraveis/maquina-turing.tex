\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Linguagens Recursivamente Enumeráveis -- Máquina de Turing}
\subtitle{Linguagens Formais, Autômatos e Computabilidade}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
 No início do século XX, diferentes matemáticos fizeram um grande esforço para
 definir um modelo computacional capaz de implementar qualquer função
 computável.
 \newline\newline
 Em 1936, Alan Turing propôs um modelo que conseguia identificar programas
 escritos para uma ``máquina universal''. Em seguida esse modelo ficou conhecido
 como \textbf{Máquina de Turing}.
\end{frame}

\begin{frame}{Introdução}
 A \textbf{Tese de Church} (ou \textbf{Hipótese de Church}), apresentada também
 em 1936, afirmou que qualquer função computável pode ser processada por uma
 \textbf{Máquina de Turing}.
 \newline\newline
 A evidência de que essa tese é verdadeira foi reforçada na medida em que os
 diferentes modelos computacionais propostos possuem, no máximo, a mesma
 capacidade computacional que a \textbf{Máquina de Turing}.
\end{frame}

\begin{frame}{Introdução}
 Resumidademente, uma \textbf{Máquina de Turing} é um autômato cuja fita não
 possui tamanho máximo e pode ser usada simultâneamente como dispositivo de
 entrada, saída, e de memória.
 \newline\newline
 Não por acaso, a \textbf{Máquina de Turing} acabou servindo de base para o
 projeto dos modernos computadores digitais.
\end{frame}

\begin{frame}{Introdução}
 As \textbf{Linguagens Recursivamente Enumeráveis} são aquelas que podem ser
 aceitas por uma \textbf{Máquina de Turing}. Como essa máquina é o mais geral
 dos dispositivos de computação, então as linguagens recursivamente enumeráveis
 representa todas as linguagens que podem ser reconhecidas mecanicamente.
\end{frame}

\begin{frame}{Introdução}
 De forma análoga às demais classes de linguagens, uma \textbf{Linguagem
 Recursivamente Enumerável} é gerada por um tipo específico de gramática, no
 caso a \textbf{Gramática Irrestrita}.
\end{frame}

\begin{frame}{Introdução}
 Há também uma outra classe de linguagens chamada \textbf{Linguagens Sensíveis
 ao Contexto}, que podem ser reconhecidas por uma \textbf{Máquina de Turing} com
 tamanho limitado na fita.
 \newline\newline
 Essas linguagens são geradas por \textbf{Gramáticas Sensíveis ao Contexto}: o
 lado esquerdo das produções pode ser uma palavra de variáveis ou terminais.
\end{frame}

\section{Máquina de Turing}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Máquina de Turing}
 Uma Máquina de Turing pode ser visualizada como uma calculadora que lê as
 entradas de uma fita, passo a passo, e pode escrever sobre esta fita e navegá-
 la para a esquerda ou direita, a depender do estado em que se encontra.
\end{frame}

\begin{frame}{Máquina de Turing}
 \begin{center}
  \includegraphics[scale=0.65]{maquina-turing-componentes.eps} 
 \end{center}
 Uma Máquina de Turing tem, portanto, 3 componentes:
 \begin{itemize}
  \item \textbf{Fita} -- usada como dispositivo de entrada, saída e memória;
  \item \textbf{Unidade de Controle} -- o estado corrente da máquina, responsável
  por realizar operações de leitura e escrita;
  \item \textbf{Função Programa} -- Função que define o estado da máquina e
  comanda as leituras, gravações e sentido do movimento.
 \end{itemize}
\end{frame}

\begin{frame}{Máquina de Turing}
 \begin{block}{Máquina de Turing}
  Um MT (Máquina de Turing) que nomearemos \textbf{M} é uma 8-upla ordenada do
  tipo:
  \begin{center}
   M = ($\sum$, Q, $\delta$, q$_0$, F, V, $\beta$, o)
  \end{center}
  Onde:
  {\footnotesize
  \begin{itemize}
   \item $\sum$ -- alfabeto de símbolos de entrada;
   \item Q -- conjunto de estados possíveis do autômato;
   \item $\delta$ -- função de transição ou programa;
   \item q$_0$ -- estado inicial, subconjunto de Q;
   \item F -- subconjunto de Q com os estados finais;
   \item V -- alfabeto auxiliar;
   \item $\beta$ -- símbolo branco na fita;
   \item o -- símbolo de início da fita.
  \end{itemize}
  }
 \end{block}
\end{frame}

\begin{frame}{Máquina de Turing}
 A Função de Transição $\delta$ de uma MT tem 2 valores como argumento: o estado
 de origem e o símbolo a ser lido da palavra em processamento.
 \newline\newline
 O resultado do processamento é uma tupla de 3 elementos: o primeiro é o estado
 resultante do processamento; o segundo informa o caractere que será escrito na
 filha; o terceiro indica qual o sentido do movimento será realizado na fita, se
 para a esquerda (``E'') ou direita (``D'').
 \begin{center}
  $\delta$(\textbf{p}, \textbf{a}) = \{(\textbf{q}, , \textbf{b}, \textbf{m})\}
 \end{center}
\end{frame}

\begin{frame}{Máquina de Turing}
 \begin{center}
  $\delta$(\textbf{p}, \textbf{a}) = \{(\textbf{q}, \textbf{b}, \textbf{m})\}
 \end{center}
 \ \newline\newline
 No exemplo da expressão acima, parte-se do estado \textbf{p} lendo-se da
 palavra o símbolo \textbf{a}. O resultado é que o estado atingido será o
 \textbf{q}, o símbolo \textbf{b} será escrito na fita, e a direção do movimento
 do processador do MT será \textbf{m}.
\end{frame}

\begin{frame}{Máquina de Turing}
 É possível que a MT não escreva nada na fita: nesse caso o símbolo que indicará
 isso será o $\varepsilon$.
\end{frame}

\begin{frame}{Máquina de Turing}
 \begin{center}
  $\delta$(\textbf{p}, \textbf{a}) = \{(\textbf{q}, \textbf{b}, \textbf{m})\}
 \end{center}
 Tomando a expressão acima como exemplo, é comum representar as transições de um
 AP da seguinte forma:
 \begin{center}
  \includegraphics[scale=0.8]{transicao.eps} 
 \end{center}
\end{frame}

\begin{frame}{Máquina de Turing}
 Após o processamento, uma MT pode atingir 3 estados possíveis:
 \newline\newline
 \textbf{Aceita}: algumas das linhas de processamento atingem um estado final;
 a palavra é aceita pela MT.
 \newline\newline
 \textbf{Rejeita}: nenhuma linha de processamento atinge um estado final; a
 palavra é rejeitada.
 \newline\newline
 \textbf{Loop}: a MT fica processando indefinidamente a partir do estado
 inicial.
\end{frame}

\section{Exemplos}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Exemplos -- Máquina de Turing}
 Vejamos nessa seção alguns exemplos de APs bem como o processamento de
 palavras.
\end{frame}

\begin{frame}{Exemplos -- Máquina de Turing}
  A Seguinte MT reconhece o duplo balanceamento entre \textbf{a} e \textbf{b}
  \begin{center}
   M$_1$ = (\{a, b\}, \{q$_0$, q$_1$, q$_2$, q$_3$, q$_4$\}, $\delta$, q$_0$,
   \{q$_4$\}, \{A, B\}, $\beta$, o)
  \end{center}
  Função de Transição:\\
  \centering
  \begin{columns}[T]
  \begin{column}{0.5\textwidth}
  $\delta$(q$_0$, o) = (q$_0$, o, D)\\
  $\delta$(q$_0$, a) = (q$_1$, A, D)\\
  $\delta$(q$_0$, B) = (q$_3$, B, D)\\
  $\delta$(q$_0$, $\beta$) = (q$_4$, $\beta$, D)\\
  $\delta$(q$_1$, a) = (q$_1$, a, D)\\
  $\delta$(q$_1$, b) = (q$_2$, B, E)\\
  $\delta$(q$_1$, B) = (q$_1$, B, D)\\
  \end{column}
  \hfill
  \begin{column}{0.5\textwidth}
  $\delta$(q$_2$, a) = (q$_2$, a, E)\\
  $\delta$(q$_2$, A) = (q$_0$, A, D)\\
  $\delta$(q$_2$, B) = (q$_2$, B, E)\\
  $\delta$(q$_2$, B) = (q$_2$, B, E)\\
  $\delta$(q$_3$, B) = (q$_3$, B, D)\\
  $\delta$(q$_3$, $\beta$) = (q$_4$, $\beta$, D)\\
  \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Exemplos -- Máquina de Turing}
  Alguns exemplos de palavras sendo processadas em M$_1$:
  \begin{columns}[T]
  \begin{column}{0.5\textwidth}
   \textbf{aabb}: -- Fita: oaabb$\beta$\\
   $\delta$(q$_0$, a) = (q$_1$, A, D) -- Fita: oAabb$\beta$\\
   $\delta$(q$_1$, a) = (q$_1$, a, D) -- Fita: oAabb$\beta$\\
   $\delta$(q$_1$, b) = (q$_2$, B, E) -- Fita: oAaBb$\beta$\\
   $\delta$(q$_2$, a) = (q$_2$, a, E) -- Fita: oAaBb$\beta$\\
   $\delta$(q$_2$, A) = (q$_0$, A, D) -- Fita: oAaBb$\beta$\\
   $\delta$(q$_0$, a) = (q$_1$, A, D) -- Fita: oAABb$\beta$\\
   $\delta$(q$_1$, B) = (q$_1$, B, D) -- Fita: oAABb$\beta$\\
   $\delta$(q$_1$, b) = (q$_2$, B, E) -- Fita: oAABB$\beta$\\
   $\delta$(q$_2$, B) = (q$_2$, B, E) -- Fita: oAABB$\beta$\\
  \end{column}
  \hfill
  \begin{column}{0.5\textwidth}
    $\delta$(q$_2$, A) = (q$_0$, A, D) -- Fita: oAABB$\beta$\\
   $\delta$(q$_0$, B) = (q$_3$, B, D) -- Fita: oAABB$\beta$\\
   $\delta$(q$_3$, B) = (q$_3$, B, D) -- Fita: oAABB$\beta$\\
   $\delta$(q$_3$, $\beta$) = (q$_4$, $\beta$, D) -- Fita: oAABB$\beta$\\
   $\delta$(q$_4$, $\beta$) = (q$_4$, $\beta$, D) -- Fita: oAABB$\beta$\\
   FIM -- Palavra Aceita
  \end{column}
  \end{columns}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
\begin{itemize}
 \item Máquina de Turing é o principal modelo teórico geral disponível sobre
 computabilidade;
 \item Máquina de Turing é capaz de reconhecer linguagens recursivamente
 enumeráveis, que representa o conjunto mais geral de computação existente;
 \item É provado, a partir da Tese (Hipótese) de Church, que se há uma função
 computável então existe uma Máquina de Turing que pode computá-la;
 \item Essas máquinas são capazes de ler e escrever em uma fita, e o
 processamento pode caminhar para a esquerda ou direita da mesma;
 \item O modelo teórico da Máquina de Turing acabou por influenciar o projeto
 dos computadores digitais modernos.
\end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
