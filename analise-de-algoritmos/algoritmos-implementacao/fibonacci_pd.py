results = {0 : 0, 1 : 1}

def fibonacci_dyn(n):

    if n in results:
        return results[n]

    results[n] = fibonacci_dyn(n - 1) + \
        fibonacci_dyn(n - 2)

    return results[n]

