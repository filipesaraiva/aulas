def buscabinaria_rec(l, x):
    mid = len(l) // 2

    if x != l[mid] and len(l) == 1:
        return False
    
    if x == l[mid]:
        return True
    elif x < l[mid]:
        return buscabinaria_rec(l[:mid], x)
    else:
        return buscabinaria_rec(l[mid:], x)
