busca([ ], A) :- false.
busca([X|XS], A) :-
    X == A, !;
    busca(XS, A).
