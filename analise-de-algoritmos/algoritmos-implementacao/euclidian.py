def euclidian(x, y):
    if y == 0:
        return x
    return euclidian(y, x % y)
