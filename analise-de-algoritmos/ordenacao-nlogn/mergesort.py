def mergesort(l):
    if len(l) <= 1:
        return l
    mid = len(l) // 2
    return merge(mergesort(l[:mid]), mergesort(l[mid:]))
