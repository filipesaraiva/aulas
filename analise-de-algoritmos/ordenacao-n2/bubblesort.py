def bubblesort(l):
    changed = True
    while changed:
        changed = False
        for i in range(1, len(l)):
            if l[i - 1] > l[i]:
                l[i - 1], l[i] = l[i], l[i - 1]
                changed = True
    return l
