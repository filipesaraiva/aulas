\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Notação Assintótica}
\subtitle{Análise de Algoritmos}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Uma Boa Métrica: Contar Número de Operações}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Contando Número de Operações}
Uma boa métrica diretamente relacionada com o algoritmo em si, que independe da
tecnologia utilizada, é a contagem do número de operações a serem realizadas.
\newline\newline
\begin{alertblock}{Atenção!}
É \textbf{contagem do número de operações a serem realizadas, não das
linhas de código!} -- esse aviso é útil para os casos em que se usam bibliotecas
de funções já implementadas.
\end{alertblock}
\end{frame}

\begin{frame}{Contando Número de Operações}
Podemos fazer certas definições para proceder a contabilização de algumas
operações e não de outras, sem risco de invalidar o embasamento teórico sobre
o tema.
\newline
\begin{itemize}
 \item Não contaremos instruções de definição de funções;
 \item Instruções que combinam operações matemáticas e atribuição serão contadas
 como se fossem apenas 1 operação;
 \item As definições e avaliações das funções de controle de fluxo serão
 contabilizadas como 1 operação para cada iteração;
 \item Estruturas de repetição são contabilizadas com 1 operação por iteração;
 \item Estruturas de repetição aninhadas normalmente serão $n^m$, onde $m$ é a
 quantidade de laços aninhados.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 1: Função para somar dois números:
\newline
\begin{lstlisting}[language=Python]
def somar(n1, n2):
    soma = n1 + n2
    return soma
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 1: Função para somar dois números:
\newline
\begin{lstlisting}[language=Python]
def somar(n1, n2):
    soma = n1 + n2    # (1)
    return soma       # (1)
\end{lstlisting}
\ \newline
Portanto, a função somar terá ($1 + 1$) 2 operações.
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 2: Algoritmo de busca do maior elemento em uma lista (fazendo 
\textit{len(lista) = n} -- tamanho da lista igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python]
def buscarMaiorElemento(lista):
    maior = lista[0]
    for i in lista:
        if maior < i:
            maior = i
    return maior
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 2: Algoritmo de busca do maior elemento em uma lista (fazendo 
\textit{len(lista) = n} -- tamanho da lista igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python]
def buscarMaiorElemento(lista):
    maior = lista[0]        # (1)
    for i in lista:         # (n)
        if maior < i:       # (n)
            maior = i       # (0 a n)
    return maior            # (1)
\end{lstlisting}
\ \newline
O número total de operações irá variar entre:\\
$1 + n + n + 0 + 1 = 2n + 2$\\ e\\ $1 + n + n + n + 1 = 3n + 2$.
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 3: Imprimir os elementos de uma matriz quadrada (fazendo 
\textit{len(matriz) = n} -- número de linhas da matriz é igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python]
def imprimirMatriz(matriz):
    for i in matriz:
        for j in i:
            print(j)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Exemplo 3: Imprimir os elementos de uma matriz quadrada (fazendo 
\textit{len(matriz) = n} -- número de linhas da matriz é igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python,mathescape=true]
def imprimirMatriz(matriz):
    for i in matriz:    # (n)
        for j in i:     # ($n^2$)
            print(j)    # ($n^2$)
\end{lstlisting}
\ \newline
O número total de operações será $n + n^2 + n^2 = 2n^2 + n$
\end{frame}

\begin{frame}{Contando Número de Operações}
Como apresentado nos exemplos 2 e 3, percebemos que o número de operações é
proporcional à entrada de dados para o algoritmo.
\newline\newline
O número de operações varia tanto pelo tamanho da entrada quanto pela disposição
de seus elementos -- por exemplo, uma lista organizada em ordem decrescente pode
implicar em diferente número de operações para ordená-la de forma crescente.
\end{frame}

\begin{frame}{Contando Número de Operações}
Algumas vantagens dessa métrica:
\newline
\begin{itemize}
 \item Intrínseca ao algoritmo;
 \item Independe de hardware, linguagem de programação, etc;
 \item Estima o ``tempo de processamento'' esperado a partir do número de
 operações;
 \item Permite a comparação entre algoritmos sem relacioná-los a características
 estranhas a eles.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Contando Número de Operações}
Variação de tempo a partir do tamanho da entrada e complexidade para um
computador que processa 1 operação a cada microssegundo:
\newline
{%
\footnotesize
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{table}
\begin{center}
\begin{tabular}{c|cccccc}
\hline
\ & \mc{6}{c}{Tamanho de \textit{n}}\\\hline
Função\\Custo & 10 & 20 & 30 & 40 & 50 & 60\\\hline
$n$ & 0,00001s & 0,00002s & 0,00003s & 0,00004s & 0,00005s & 0,00006s\\
$n^2$ & 0,0001s & 0,0004s & 0,0009s & 0,0016s & 0,0035s & 0,0036s\\
$n^3$ & 0,001s & 0,008s & 0,027s & 0,64s & 0,125s & 0,316s\\
$n^5$ & 0,1s & 3,2s & 24,3s & 1,7min & 5,2min & 13min\\
$2^n$ & 0,001s & 1s & 17,9min & 12,7dias & 35,7anos & 366séc.\\
$3^n$ & 0,059s & 58min & 6,5anos & 3855séc. & $10^8$séc. & $10^{13}$séc.\\\hline
\end{tabular}
\end{center}
\end{table}
}%
\end{frame}

\section{Notação Assintótica}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Notação Assintótica}
Como o número de operações realizadas depende da entrada, qual quantidade de
operações devemos assumir como o valor da métrica para um dado algoritmo?
\end{frame}

\begin{frame}[fragile]{Notação Assintótica}
Exemplo 2: Algoritmo de busca do maior elemento em uma lista (fazendo 
\textit{len(lista) = n} -- tamanho da lista igual a \textit{n}):
\newline
\begin{lstlisting}[language=Python]
def buscarMaiorElemento(lista):
    maior = lista[0]        # (1)
    for i in lista:         # (n)
        if maior < i:       # (n)
            maior = i       # (0 a n)
    return maior            # (1)
\end{lstlisting}
\ \newline
O número total de operações irá variar entre:\\
$1 + n + n + 0 + 1 = 2n + 2$\\ e\\ $1 + n + n + n + 1 = 3n + 2$.
\end{frame}

\begin{frame}{Notação Assintótica}
Como o número de operações realizadas depende da entrada, qual quantidade de
operações devemos assumir como o valor da métrica para um dado algoritmo?
\newline\newline
É possível utilizar tanto as de melhor caso (menor número de operações) quanto
as do caso médio ou pior caso;
\newline
\begin{exampleblock}{Qual utilizar então?}
Na área é mais comum utilizarmos a \textbf{medição no pior caso}.
\end{exampleblock}
\end{frame}

\begin{frame}{Notação Assintótica}
Chama-se \textbf{Complexidade Assintótica} a complexidade do algoritmo no pior
caso. Por que ela é a mais utilizada?
\newline
\begin{itemize}
 \item Normalmente, se um algoritmo se comporta bem para o pior caso, ele se
 comportará bem também nos melhores;
 \item Nos faz projetar os algoritmos com maior zelo, levando-se em conta que o
 pior caso pode ocorrer.
\end{itemize}
\end{frame}

\begin{frame}{Notação Assintótica}
A notação da complexidade assintótica chama-se notação assintótica, representada
por \textbf{O(função)}.
\newline\newline
Para calcularmos a função de complexidade, procedemos com o cálculo do número de operações para o pior caso e utilizamos apenas o índice de maior grau do
polinômio.
\end{frame}

\begin{frame}[fragile]{Notação Assintótica}
Exemplo 4: Algoritmo de ordenação \textit{Selection Sort}:
\newline
\begin{lstlisting}[language=Python]
def selectionsort(l):
    for i in range(0, len(l)):
        x = i
        for j in range(i + 1, len(l)):
            if l[x] > l[j]:
                x = j
        l[i], l[x] = l[x], l[i]
    return l
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Notação Assintótica}
Exemplo 4: Algoritmo de ordenação \textit{Selection Sort}:
\newline
\begin{lstlisting}[language=Python,mathescape=true]
def selectionsort(l):
    for i in range(0, len(l)):            # (n)
        x = i                             # (n)
        for j in range(i + 1, len(l)):    # ($n^2$)
            if l[x] > l[j]:               # ($n^2$)
                x = j                     # ($n^2$)
        l[i], l[x] = l[x], l[i]           # (n)
    return l                              # (1)
\end{lstlisting}
\ \newline
Número total de operações: $n + n + n^2 + n^2 + n^2 + n + 1 = 3n^2 + 3n + 1$\\
Complexidade: O($n^2$)
\end{frame}

\begin{frame}{Notação Assintótica}
Matematicamente, o significado da complexidade assintótica diz respeito à
dominação que uma função tem sobre outra em termos das entrada.
\newline\newline
Uma função $f(n)$ é dominado assintoticamente por $g(n)$ se existem duas
constantes positivas $c$ e $m$ tais que:
\newline
\begin{center}
para $n \geq m$, temos $| f(n) | \leq c | g(n) |$
\end{center}
\end{frame}

\begin{frame}{Notação Assintótica}
A função \textit{x}, azul, é dominada assintoticamente por \textit{x$^2$}, 
verde, a partir de $x > 1$.
\begin{center}
\includegraphics[scale=0.35]{x-x2.eps}
\end{center}
\end{frame}

\begin{frame}{Notação Assintótica}
Classes de complexidade O típicas:
\newline
\begin{itemize}
 \item \textbf{O(1)} -- complexidade constante: o algoritmo independe da
 entrada;
 \item \textbf{O($\mathbf{log_2}$ n)} -- complexidade logarítimica: muito comum em problemas
 que particionam a entrada;
 \item \textbf{O(n)} -- complexidade linear: a complexidade cresce linearmente
 de acordo com a entrada;
 \item \textbf{O(n $\mathbf{log_2}$ n)} -- encontrada quando algoritmos dividem um problema e
 depois reúne as soluções;
\end{itemize}
\end{frame}

\begin{frame}{Notação Assintótica}
Classes de complexidade O típicas (continuação):
\newline
\begin{itemize}
 \item $\mathbf{O(n^2)}$ -- complexidade quadrática: muito encontrado em
 processamento de matrizes;
 \item $\mathbf{O(n^3)}$ -- complexidade cúbica: só deve ser utilizada para
 resolver problemas pequenos;
 \item $\mathbf{O(2^n)}$ -- complexidade exponencial: comum em algoritmos de
 força bruta;
 \item $\mathbf{O(n!)}$ -- complexidade fatorial: também muito comum em
 algoritmos de força bruta, mas muito pior que a complexidade $O(2^n)$;
\end{itemize}
\end{frame}

\begin{frame}{Notação Assintótica}
Ordenação das complexidades O em ordem crescente: 
\newline\newline
\begin{center}
 $\mathbf{O(1)} < \mathbf{O(log_2n)} < \mathbf{O(n)} < \mathbf{O(n\ log_2n)}
< \mathbf{O(n^2)} < \mathbf{O(n^3)} < \mathbf{O(2^n)} < \mathbf{O(n!)}$
\end{center}
\ \newline\newline
O avanço tecnológico dos computadores seria suficiente para resolver problemas
com algoritmos de complexidade elevada?
\end{frame}

\begin{frame}[fragile]{Notação Assintótica}
Variação de tempo a partir do tamanho da entrada e complexidade para um
computador que processa 1 operação a cada microssegundo:
\newline
{%
\footnotesize
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{table}
\begin{center}
\begin{tabular}{c|cccccc}
\hline
\ & \mc{6}{c}{Tamanho de \textit{n}}\\\hline
Função\\Custo & 10 & 20 & 30 & 40 & 50 & 60\\\hline
$n$ & 0,00001s & 0,00002s & 0,00003s & 0,00004s & 0,00005s & 0,00006s\\
$n^2$ & 0,0001s & 0,0004s & 0,0009s & 0,0016s & 0,0035s & 0,0036s\\
$n^3$ & 0,001s & 0,008s & 0,027s & 0,64s & 0,125s & 0,316s\\
$n^5$ & 0,1s & 3,2s & 24,3s & 1,7min & 5,2min & 13min\\
$2^n$ & 0,001s & 1s & 17,9min & 12,7dias & 35,7anos & 366séc.\\
$3^n$ & 0,059s & 58min & 6,5anos & 3855séc. & $10^8$séc. & $10^{13}$séc.\\\hline
\end{tabular}
\end{center}
\end{table}
}%
\end{frame}

\begin{frame}[fragile]{Notação Assintótica}
Com um computador mais rápido posso resolver problemas com algoritmos de
complexidade elevada?
\newline
{%
\footnotesize
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{table}
\begin{center}
\begin{tabular}{c|ccc}
\hline
\ & \mc{3}{c}{Tamanho possível de ser resolvido em um tempo $t_x$}\\\hline
\specialcell{Função\\Custo} & \specialcell{Computador\\ Atual} &
\specialcell{Computador\\ 100x mais rápido} & \specialcell{Computador\\ 1000x
mais rápido} \\\hline
$n$ & $t_1$ & $100t_1$ & $1000t_1$\\
$n^2$ & $t_2$ & $10t_2$ & $31,6t_2$ \\
$n^3$ & $t_3$ & $4,6t_3$ & $10t_3$\\
$2^n$ & $t_4$ & $t_4 + 6,6$ & $t_4 + 10$\\\hline
\end{tabular}
\end{center}
\end{table}
}%
\end{frame}

\begin{frame}[fragile]{Notação Assintótica}
\begin{center}
 \includegraphics[scale=0.45]{big-o.png}
\end{center}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}[fragile]{Conclusões}
\begin{itemize}
 \item Melhor maneira de comparar algoritmos é a partir do comportamento dos
 mesmos na entrada de pior caso -- análise assintótica, ou Big O;
 \item Para calcular, usa-se o componente de maior grau do polinômio extraído da
 soma das possíveis operações;
 \item Algoritmos de complexidade muito elevada podem tornar problemas
 impossíveis de serem resolvidos na prática.
\end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
