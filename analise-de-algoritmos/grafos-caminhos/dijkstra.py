g = [[0,  0, 17, 5, 15, 0],
     [0,  0, 0,  0, 7,  3],
     [17, 0, 0,  0, 2,  0],
     [5,  0, 0,  0, 0,  4],
     [15, 7, 2,  0, 0,  21],
     [0,  3, 0,  4, 21,  0]]

def dijkstra(g, noFonte, noDestino):

    import numpy
    inf = numpy.inf

    custo = [inf for i in range(6)]
    anterior = [0 for i in range(6)]
    avaliado = [False for i in range(6)]

    custo[noFonte] = 0
    while avaliado[noDestino] != True:

        minIndiceCusto = 0
        for i in range(len(custo)):
            if custo[i] < custo[minIndiceCusto] and not avaliado[i]:
                minIndiceCusto = i

        for i in range(len(g)):
            if avaliado[i] == False and g[minIndiceCusto][i] > 0 and custo[i] > custo[minIndiceCusto] + g[minIndiceCusto][i]:
                custo[i] = custo[minIndiceCusto] + g[minIndiceCusto][i]
                anterior[i] = minIndiceCusto

        avaliado[minIndiceCusto] = True
        custo[noFonte] = inf

    solucao = [noDestino]
    noAnterior = noDestino
    while noAnterior != noFonte:
        noAnterior = anterior[noAnterior]
        solucao.insert(0, noAnterior)

    return solucao
