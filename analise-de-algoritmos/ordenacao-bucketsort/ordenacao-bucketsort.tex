\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Bucket Sort: Um Algoritmo O(n$^2$) Especial}
\subtitle{Análise de Algoritmos}
\institute{UFPA}
\date{\today}

\begin{document}    

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
Estudaremos nessa aula um algoritmo O(n$^2$) especial chamado Bucket Sort. Esse
algoritmo muitas vezes é descrito como de ``ordenação externa'' e tem uma
explicação visual de simples entendimento.
\newline\newline
Na apresentação veremos que o Bucket Sort tem complexidade quadrática no pior
caso, mas é possível desenvolver uma versão O(n) a custo de maior utilização de
memória.
\end{frame}

\section{Bucket Sort}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Bucket Sort -- Introdução}
O Bucket Sort é um algoritmo que se utiliza do projeto de algoritmo ``Dividir
para Conquistar'', dividindo o problema original (ou seja, a lista original) em
subproblemas menores (significa, em sublistas menores) para então resolvê-los
(ordenar as sublistas) e reunir essas resoluções que por consequência será a
resolução do problema original (a ordenação da lista original).
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
No Bucket Sort os elementos da lista original são divididos em listas menores,
que compreendem determinados intervalos de valores dentro de certo limite.
\newline\newline
Por exemplo, podemos supôr uma lista com 100 elementos variando de 0 a 100. O
algoritmo Bucket Sort poderia dividir esses elementos em 10 sublistas, onde a
lista 1 compreenderia os elementos entre 0 e 10, a lista 2 os elementos entre 11
e 20, a 3 entre 21 e 30, e assim sucessivamente.
\newline\newline
Após dividir a lista original, um outro algoritmo de ordenação é aplicado a cada
sublista. Com elas ordenadas, basta concatená-las para termos o resultado do
problema original.
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
A palavra ``Bucket'' em português significa ``balde''. Essa é a metáfora do
algoritmo: dividir a lista original em ``baldes'' diferentes e depois ordená-las
separadamente.
\newline\newline
Na imagem a seguir temos a lista original sendo dividida em 3 ``baldes'': o
primeiro aceita elementos entre 0 e 5; o segundo entre 6 e 10; e o último entre
11 e 15.
 \begin{center}
 \includegraphics[scale=0.5]{bucket.eps}
 \end{center}
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
Para que o Bucket Sort tenha um bom desempenho, é necessário decidir quantos
``baldes'' serão utilizados e quais intervalos eles compreendem. O sucesso desse
método será obtido quando os baldes dividirem a lista original de forma
equilibrada, com quantidades similares de elementos em cada um. Nesse caso, o
algoritmo fará menos iterações, obtendo um bom desempenho.
\newline\newline
Outro fator determinante no desempenho do algoritmo é qual algoritmo de
ordenação será empregado para ordenar os ``baldes''.
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
Não por acaso, pelo fato do desenvolvedor poder optar por um algoritmo
quadrático para ordenar os ``baldes'' faz com que o Bucket Sort tenha
complexidade O(n$^2$): afinal, pode ser utilizado tanto o Bubble Sort, Insertion
Sort ou Selection Sort para essa tarefa.
\newline\newline
Por outro lado, se o desenvolvedor optar por utilizar \textbf{1 balde para cada
tipo de elemento da lista original}, o Bucket Sort terá complexidade $\Omega$(n)
 -- esse é o melhor caso do algoritmo.
\newline\newline 
Entendemos que a versão O(n$^2$) do Bucket Sort é de simples implementação. A
seguir será apresentada a versão $\Omega$(n) do algoritmo.
\end{frame}

\begin{frame}[fragile]{Bucket Sort -- Algoritmo}
  \lstinputlisting[language=Python]{bucketsort.py}
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
Na primeira instrução do método, \textbf{buckets = [[]] * (max(l) + 1)}, é
criada uma lista de listas (\textit{buckets}) de número igual ao maior elemento
da lista original $l$ + 1. Vale ressaltar que a chamada à função \textit{max}
tem complexidade O(n) já.
\newline\newline
Usando a lista da ilustração como exemplo, $l = [3, 5, 15, 10, 7, 2, 11, 12]$,
o maior elemento será $15$ e por consequência $buckets$ terá o seguinte valor
(1 lista com 16 sublistas vazias):
\newline\newline
buckets = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
O primeiro bloco \textbf{for} separará os elementos da lista original nos seus
respectivos buckets.
\newline\newline
Assim, após o fim desse bloco, $buckets$ terá a seguinte forma:
\newline\newline
buckets = [[], [], [2], [3], [], [5], [], [7], [], [], [10], [11], [12], [], [],
[15]]
\newline\newline
Perceba que há diversas sublistas vazias, pois não havia elementos na lista
original para essas posições. Isso significa que o Bucket Sort aloca memória
que pode ser não utilizada, um problema também enfrentado pelo algoritmo
Counting Sort.
\end{frame}

\begin{frame}{Bucket Sort -- Explicação}
Divididos os elementos, agora basta concatená-los para que a resolução do
problema original seja obtida.
\newline\newline
Uma nova lista $x$ vazia é criada e o próximo bloco \textbf{for} tratará de
varrer a lista $buckets$ e ir concatenando cada sublista à lista $x$. Ao final,
na saída desse \textbf{for}, a lista $x$ será a lista original $l$ ordenada.
\newline\newline
x = [2, 3, 5, 7, 10, 11, 12, 15]
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item Nessa apresentação foi discutido o algoritmo Bucket Sort, um algoritmo
  de ``ordenação externa'' que tem custo O(n$^2$);
  \item Apesar disso, é possível reduzir a complexidade no melhor caso para
  $\Omega$(n), dividindo a lista original em uma sublista para cada tipo de
  elemento;
  \item O ruim dessa abordagem é que o Bucket Sort pode exigir muita memória
  para esse tipo de situação.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
