\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{algorithmic}
\usepackage{textcomp}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\input{algorithmic-portuguese}

%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Busca Tabu}
\subtitle{Inteligência Artificial}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
  Nessa apresentação veremos os conceitos fundamentais da metaheurística
  \textbf{Busca Tabu}, do inglês \textit{Tabu Search} (TS).
\end{frame}

\section{Busca Tabu}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Busca Tabu}
  A Busca Tabu foi proposta por Glover em 1986. A ideia inicial era aumentar a
  variabilidade da busca empreendida pelo Arrefecimento Simulado de forma a
  reforçar a etapa de diversificação da busca.
\end{frame}

\begin{frame}{Busca Tabu}
  A Busca Tabu faz analogia com procedimentos de memorização. Durante a busca,
  mudanças que transformam uma solução em outra são salvas e impedidas de serem
  realizadas em um certo número de próximas iterações, servindo de guia para a
  busca empreendida pelo método.
\end{frame}

\begin{frame}{Busca Tabu}
  Após a publicação do método e a obtenção de resultados promissores nas
  primeiras aplicações, o TS tornou-se muito popular sendo utilizado tanto
  de forma única quanto como componente em métodos metaheurísticos populacionais
  ou em etapas de métodos exatos.
\end{frame}

\section{Etapas da Busca Tabu}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Etapas da Busca Tabu}
  Detalharemos a seguir as principais etapas do TS, a saber:
  \begin{itemize}
   \item Definição da Lista Tabu;
   \item Geração da Solução Inicial;
   \item Função de Vizinhança;
   \item Seleção do Vizinho;
   \item Critério de Aspiração;
   \item Memória de Médio Prazo;
   \item Memória de Longo Prazo;
   \item Critério de parada.
  \end{itemize}
\end{frame}

\begin{frame}{Definição da Lista Tabu}
  Uma das principais características do TS é a presença de uma estrutura chamada
  Lista Tabu.
  \newline\newline
  A Lista Tabu memoriza as alterações realizadas entre uma solução em avaliação
  e o vizinho assumido durante o processo de busca e impede que essa modificação
  seja realizada novamente por um certo número de iterações posteriores.
\end{frame}

\begin{frame}{Definição da Lista Tabu}
  Imagine que para o Problema da Mochila 0-1 com 5 itens temos uma dada solução
  avaliada e o vizinho assumido apresentados, respectivamente, abaixo:
  \begin{center}
   0 1 1 \textbf{0} 1\\
   0 1 1 \textbf{1} 1
  \end{center}
  Percebe-se que o elemento 4 foi alterado nas soluções. Essa modificação
  poderia então ser adicionada à Lista Tabu, evitando que o elemento 4 fosse
  novamente alterado por um certo número de iterações.
\end{frame}

\begin{frame}{Definição da Lista Tabu}
  Após atingir o número definido de iterações, o registro de alteração no índice
  4 sai da Lista Tabu, permitindo que novos vizinhos sejam gerados com alteração
  nesse índice.
  \newline\newline
  Esse artifício permite a diversificação na Busca Tabu, pois evitando
  alterações registradas na lista acaba por guiar a busca para novos subespaços,
  evitando a convergência antecipada para um ótimo local.
\end{frame}

\begin{frame}{Definição da Lista Tabu}
  O tamanho da Lista Tabu é um parâmetro que varia para cada aplicação e
  problema, sendo portanto uma decisão de implementação do projetista.
  \newline\newline
  Também cabe comentar que a Lista Tabu, apesar do nome, se assemelha mais a
  uma fila, onde registros antigos são removidos enquanto novos são adicionados
  à estrutura.
\end{frame}

\begin{frame}{Geração da Solução Inicial}
  Como um método que trabalha a partir de uma solução inicial, o TS precisa
  gerar essa solução de alguma forma.
  \newline\newline
  Quando não se conhece a instância do problema a ser trabalhada, no geral essa
  solução é criada de forma aleatória. Com algum conhecimento do problema, é
  possível definir partes da solução e gerar outras de forma aleatória, mas isso
  nem sempre é possível.
\end{frame}

\begin{frame}{Função de Vizinhança}
  Como toda metaheurística, o TS precisa de uma função para gerar a vizinhança e
  proceder a busca.
  \newline\newline
  O TS irá gerar um subconjunto de vizinhos e a partir deles procederá a
  seleção para saber para qual vizinho a busca será direcionada.
  \newline\newline
  É importante que a Função de Vizinhança seja escolhida de forma bem embasada
  pois a mesma é das principais responsáveis pela busca do método.
\end{frame}

\begin{frame}{Função de Vizinhança}
  O tamanho do subconjunto gerado também deve ser avaliado para evitar que o TS
  gere um número muito grande de soluções vizinhas.
  \newline\newline
  Também é importante que a Função de Vizinhança tenha alguma componente
  aleatória para permitir que uma mesma solução possa atingir diferentes
  subconjuntos.
\end{frame}

\begin{frame}{Seleção do Vizinho}
  Gerados os vizinhos, o método ordena-os a partir daquele que tem o melhor
  desempenho na Função Objetivo para o pior.
  \newline\newline
  A partir do melhor, verifica-se se o movimento que o gerou não está presente
  na Lista Tabu:
  \begin{itemize}
   \item Caso \textbf{Não}: o método assume essa nova solução e o  movimento que
   a gerou é adicionado à Lista Tabu;
   \item Caso \textbf{Sim}: avalia-se a próxima solução, seguindo a ordem. Caso
   todas as soluções tenham sido avaliadas, a busca permanece na mesma solução e
   gera-se uma nova vizinhança.
  \end{itemize}
\end{frame}

\begin{frame}{Seleção do Vizinho}
  Como durante a busca o TS assume novas soluções, descartanto as anteriores,
  inclusive assumindo soluções piores que a em avaliação, é imprescindível
  sempre memorizar a melhor solução encontrada durante a execução do método.
\end{frame}

\begin{frame}{Critério de Aspiração}
  Uma flexibilização à etapa de Seleção do Vizinho acontece quando é gerada uma
  boa solução porém o movimento que a gerou está na Lista Tabu.
  \newline\newline
  Nos casos em que essa solução é melhor que a melhor solução encontrada até o
  momento, pode-se adotar um critério de desconsiderar a Lista Tabu, ou
  verificar alguma porcentagem, para mover a busca para essa solução.
  \newline\newline
  A esse artifício chama-se \textbf{Critério de Aspiração}.
\end{frame}

\begin{frame}{Memórias de Médio e Longo Prazo}
  A Busca Tabu básica compreende apenas as etapas anteriormente descritas mais o
  critério de parada. Entretanto, estudos posteriores adicionaram ao método mais
  duas etapas, as chamadas Memória de Médio e Memória de Longo Prazo.
  \newline\newline
  Para efeito de nomeclatura, a Lista Tabu é tida como a Memória de Curto Prazo
  do TS.
\end{frame}

\begin{frame}{Memória de Médio Prazo}
  Na \textbf{Memória de Médio Prazo}, o TS guarda as componentes mais comuns
  encontradas nas melhores soluções.
  \newline\newline
  Caso o TS realize um certo número de iterações sem melhorias, o método pode
  reiniciar a busca com uma nova solução que contenha esses componentes mais
  comuns.
  \newline\newline
  A Memória de Médio Prazo está relacionada com a intensificação na busca do TS.
\end{frame}

\begin{frame}{Memória de Longo Prazo}
  Já na \textbf{Memória de Longo Prazo} o TS memoriza a frequência com as quais
  as componentes são encontradas em \textbf{todas} as soluções visitadas.
  \newline\newline
  Caso o TS realize um certo número de iterações sem melhorias, o método pode
  reiniciar a busca com uma solução que utiliza as configurações mais frequentes
  memorizadas.
  \newline\newline
  A Memória de Longo Prazo está relacionada com a diversificação na busca do TS.
\end{frame}

\begin{frame}{Critério de Parada}
  O critério de parada mais utilizado para o TS é quando o método atinge um
  número máximo de iterações ou quando executa em sequência um certo número de
  iterações sem melhoria na melhor solução encontrada.
\end{frame}

\section{Pseudocódigo}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Pseudocódigo}
  Os principais parâmetros do TS que devem ser decididos durante o projeto do
  método são:
  \begin{itemize}
   \item Tamanho da Lista Tabu;
   \item Geração dos vizinhos;
   \item Número de vizinhos a serem gerados;
   \item Implementação da memória de médio prazo;
   \item Implementação da memória de longo prazo;
   \item Critério de parada.
  \end{itemize}
  A seguir tem-se o pseudocódigo para o TS.
\end{frame}

\begin{frame}[fragile]{Pseudocódigo}
  
  \begin{algorithmic}
   {\footnotesize
   \STATE Gera solução inicial
   \REPEAT
    \STATE Gera vizinhos
      \IF{Melhor vizinho é a melhor solução encontrada}
        \STATE Assume vizinho como nova solução (Critério de Aspiração)
        \STATE Continua
      \ENDIF
    \FOR{Vizinhos ordenados do melhor para o pior}
      \IF{Alteração no vizinho não está na Lista Tabu}
        \STATE Assume vizinho como nova solução
        \STATE Atualiza Lista Tabu
        \STATE Break
      \ENDIF
    \ENDFOR
    \IF{Várias iterações sem alteração na melhor solução}
      \STATE Aplica Memória de Médio ou Longo Prazo
    \ENDIF
   \UNTIL{Atingir critério de parada}
   }
  \end{algorithmic}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item O TS é uma metaheurística de solução única baseada em processos de
  memorização;
  \item No TS, o processo de busca vai salvando alterações de soluções em uma
  Lista Tabu evitando que aquela mudança ocorra novamente em um certo número de
  iterações;
  \item O TS também conta com artifícios que simulam memórias de médio e longo
  prazo, tentando aumentar a diversidade ou a intensificação a busca.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
