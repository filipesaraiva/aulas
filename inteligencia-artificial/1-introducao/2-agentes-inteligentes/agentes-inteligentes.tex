\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Agentes Inteligentes}
\subtitle{Inteligência Artificial}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
  No capítulo anterior foi apresentada a área de Inteligência Artificial, sua
  evolução histórica e alguns dos principais problemas e técnicas presentes
  hoje.
  \newline\newline
  Nessa aula veremos o conceito de \textbf{agentes inteligentes}, que estrutura
  as diferentes aplicações em IA e nos possibilita modelá-las seguindo uma
  ``arquitetura'' comum.
\end{frame}

\section{Agentes Inteligentes}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\subsection{O que são Agentes}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{O que são Agentes}
  \begin{minipage}{\textwidth}
   \begin{minipage}{0.5\textwidth}
    Um \textbf{agente} é tudo o que pode ser considerado capaz de perceber seu
    \textbf{ambiente} por meio de \textbf{sensores} e agir sobre esse ambiente por
    meio de \textbf{atuadores}.
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \begin{center}
     \includegraphics[scale=0.35]{agente.png}
    \end{center}
  \end{minipage}
  \end{minipage}
\end{frame}

\begin{frame}{O que são Agentes}
  Os \textbf{sensores} são uma metáfora para tudo aquilo que permite ao agente
  obter dados do seu ambiente de aplicação.
  \begin{itemize}
   \item Termômetro;
   \item Sensor de umidade;
   \item Câmeras;
   \item Mapeamento de peças em um tabuleiro;
   \item Botões de um software para entrada de dados;
   \item Arquivos de texto ou imagens
   \item ...
  \end{itemize}
\end{frame}

\begin{frame}{O que são Agentes}
  Já os \textbf{atuadores} permitem ao agente interagir com o ambiente,
  modificando-o. Destaca-se que em muitos casos o atuador apenas ``imprime o
  resultado na tela'', o que é comum para diversas aplicações de agentes.
  \begin{itemize}
   \item Pés, pernas ou rodas;
   \item Direção, volante;
   \item Motores;
   \item Telas, monitores;
   \item ...
  \end{itemize}
\end{frame}

\begin{frame}{O que são Agentes}
  A partir dessa espécie de ``modelo arquitetural'', podemos pensar diferentes
  aplicações como agentes:
  \begin{itemize}
   \item Carro autônomo;
   \item Jogo de xadrez;
   \item Robô de resgate;
   \item \textit{Scrapper} de redes sociais;
   \item Classificador de imagem;
   \item Otimização de entregas logísticas;
   \item ...
  \end{itemize}
\end{frame}

\begin{frame}{O que são Agentes}
  Em um agente, existe um mapeamento entre as percepções captadas do ambiente e
  a atuação do agente sobre o mesmo, guiada pelo objetivo que modela.
  \newline\newline
  A esse mapeamento chamamos \textbf{função agente}, e ela é implementada
  no que convenciona-se chamar \textbf{programa de agente}.
\end{frame}

\begin{frame}{O que são Agentes}
  \begin{center}
    \includegraphics[scale=0.45]{aspirador-de-po.png}
  \end{center}
  Tomemos o exemplo de um agente aspirador de pó que deve limpar um ambiente.
  \newline\newline
  Façamos com que esse ambiente seja dividido em 2 partes, A e B.
\end{frame}

\begin{frame}{O que são Agentes}
  \begin{center}
    \includegraphics[scale=0.45]{aspirador-de-po.png}
  \end{center}
  Qual ambiente?
  \begin{itemize}
   \item Um cômodo dividido em parte A e B;
   \item As partes podem estar sujas ou não.
  \end{itemize}
  Quais sensores?
  \begin{itemize}
   \item Verificar sujeira no local onde está.
  \end{itemize}
  Quais atuadores?
  \begin{itemize}
   \item Aspirador de sujeira;
   \item Deslocar-se de um local para o outro.
  \end{itemize}
\end{frame}

\begin{frame}{O que são Agentes}
  \begin{center}
    \includegraphics[scale=0.45]{aspirador-de-po.png}
  \end{center}
  Exemplo de função agente para o aspirador de pó:
  \begin{center}
  \begin{tabular}{cc}
  \textbf{Percepções} & \textbf{Ação}\\
  $\left[A, Limpo\right]$ & Direita\\
  $\left[A, Sujo\right]$ & Aspirar\\
  $\left[B, Limpo\right]$ & Esquerda\\
  $\left[B, Sujo\right]$ & Aspirar\\
  $\left[A, Limpo\right]$, $\left[A, Limpo\right]$ & Direita\\
  $\left[A, Limpo\right]$, $\left[A, Sujo\right]$ & Aspirar\\
  ... & ...
  \end{tabular}
  \end{center}  
\end{frame}

\begin{frame}{O que são Agentes}
  \begin{center}
  \begin{tabular}{cc}
  \textbf{Percepções} & \textbf{Ação}\\
  $\left[A, Limpo\right]$ & Direita\\
  $\left[A, Sujo\right]$ & Aspirar\\
  $\left[B, Limpo\right]$ & Esquerda\\
  $\left[B, Sujo\right]$ & Aspirar\\
  $\left[A, Limpo\right]$, $\left[A, Limpo\right]$ & Direita\\
  $\left[A, Limpo\right]$, $\left[A, Sujo\right]$ & Aspirar\\
  ... & ...
  \end{tabular}
  \end{center}  
  A tabela acima da função agente pode ser preenchida de diversas maneiras. A
  pergunta que fica é, \textbf{qual a maneira correta de preencher a tabela?}
\end{frame}

\subsection{Racionalidade e Medida de Desempenho}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  Nesse momento cabe observar que aplicações de IA não dizem respeito apenas a
  fazer algo - o sistema deve \textbf{desempenhar a tarefa o melhor possível}.
  \begin{itemize}
   \item Um carro autônomo não deve apenas dirigir - deve evitar acidentes,
   utilizar bem o combustível, ...
   \item Um sistema de jogo de xadrez não deve apenas movimentar as peças de
   qualquer forma - deve ter estratégias para vencer o adversário;
   \item Um sistema classificador de imagens deve ter alta precisão para
   realizar a tarefa com a menor quantidade de erros possível.
  \end{itemize}
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  Chamamos de \textbf{agente racional} aquele que faz tudo certo.
  \newline\newline
  Isso significa que em termos conceituais, todas as percepções do agente
  refletem a melhor ação possível na tabela da função agente.
  \newline\newline
  Entretanto, o que é ``fazer tudo certo?''
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  Podemos dizer que a ação certa é aquela que fará o agente obter o maior
  sucesso.
  \newline\newline
  Assim, precisaremos de uma \textbf{medida de desempenho} para avaliar o quão
  bom cada possível ação que um agente possa tomar é em relação às demais.
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  A medida de desempenho, portanto, mede o ``sucesso'' do agente em sua
  aplicação.
  \newline\newline
  Dada as percepções do agente, suas ações realizada modificam o ambiente. Se
  essa modificação é desejável, então o agente funcionou bem.
  \newline\newline
  Quando a sequência de ações de um agente é melhor que outro, para um mesmo
  problema, identificamos que aquele agente teve desempenho melhor.
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  Definir uma medida de desempenho é uma tarefa em si complexa.
  \newline\newline
  Por exemplo, a aplicação do robô aspirador de pó:
  \begin{itemize}
   \item Quantidade de sujeira limpa por turno: o robô poderia ficar
   gastando energia à toa procurando sujeira?
   \item Quantidade de quadrados limpos ao final de um turno: o robô poderia
   limpar apenas próximo do final do turno?
   \item Menor gasto possível de energia: o robô poderia ficar parado?
  \end{itemize}
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  Retomando ao exemplo do robô aspirador, vamos supôr:
  \begin{itemize}
   \item A medida de desempenho oferece um prêmio de 1 ponto para cada quadrado
   limpo em cada período de tempo - duração de 1000 tempos;
   \item O ambiente é conhecido à priori mas a sujeira não. O agente precisa
   de vez em quando navegar no ambiente para verificar se está sujo;
   \item Ações possíveis: \textit{Esquerda}, \textit{Direita}, \textit{Aspirar},
   \textit{NoOp};
   \item O agente percebe corretamente sua posição e se ela está suja.
  \end{itemize}
  Esse é um exemplo de agente racional.
\end{frame}

\begin{frame}{Racionalidade e Medida de Desempenho}
  Finalizando, cabe comentar que há agentes que \textbf{aprendem} com a
  experiência, e podem ir se tornando mais inteligentes na realização de suas
  tarefas.
  \newline\newline
  Nem sempre o aprendizado é algo que se espera de agentes, mas a depender da
  aplicação essa característica pode ser fundamental.
\end{frame}

\subsection{Ambientes}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Ambientes}
  Com o agente definido, seus sensores, atuadores, e medida de desempenho
  especificadas, é necessário determinar em paralelo onde esse agente irá
  executar.
  \newline\newline
  Chama-se \textbf{ambiente} o local onde o agente estará imergido ou mesmo o
  problema para o qual o agente é a ``solução''.
\end{frame}

\begin{frame}{Ambientes}
  \begin{center}
    \includegraphics[scale=0.45]{aspirador-de-po.png}
  \end{center}
  No exemplo do aspirador de pó, o ambiente é composto por:
  \begin{itemize}
   \item 2 locais (A e B);
   \item Esses locais podem estar limpos ou com sujeira.
  \end{itemize}
  Trata-se de um ambiente simples e didático.
\end{frame}

\begin{frame}{Ambientes}
  \begin{center}
    \includegraphics[scale=0.4]{carro-autonomo.jpg}
  \end{center}
  E um ambiente mais complexo como o de um carro autônomo?
\end{frame}

\begin{frame}{Ambientes}
  \begin{center}
    \includegraphics[scale=.38]{lidar.jpg}
  \end{center}
  \href{https://www.youtube.com/watch?v=nXlqv_k4P8Q}{Vídeo: O mundo visto por um
  carro autônomo}
\end{frame}

\begin{frame}{Ambientes}
  Ambiente para um carro autônomo:
  \begin{itemize}
   \item Extremamente aberto: sem limites para combinações de circunstâncias que
   possam vir;
   \item Diversos tipos de estradas - rurais, avenidas urbanas,
   ``\textit{autobahns}'', ...;
   \item Diversos tipos de tráfego: pedestres, animais, trabalhadores, polícia,
   ambulância, poças, buracos, alagamentos, ...;
   \item Clima;
   \item Interação com outros veículos;
   \item Legislação de trânsito;
   \item... 
  \end{itemize}
\end{frame}

\begin{frame}{Ambientes}
  Ambiente para um carro autônomo:
  \begin{itemize}
   \item Qual medida de desempenho? Um combinado de:
   \begin{itemize}
    \item Chegar ao destino correto;
    \item Minimizar o consumo de combustível;
    \item Minimizar o tempo e custo da viagem;
    \item Minimizar as violações às leis de trânsito;
    \item Maximizar a segurança e conforto dos passageiros;
    \item ...
   \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Ambientes}
  Para atingir os objetivos no ambiente, o agente deve ser projetado para ter
  sensores e atuadores condizentes com os mesmos.
  \newline\newline
  Para o exemplo do carro autônomo, além dos sensores esperados de agentes
  ``humanos'', espera-se ter acesso a um GPS, velocímetro, sensor de
  combustível, base de dados da lei de trânsito, e outros.
  \newline\newline
  Já os atuadores seriam o acelerador, marchas, freios, volante, buzina,
  e outros.
\end{frame}

\begin{frame}{Ambientes}
  Algumas características que definem um ambiente:
  \begin{itemize}
   \item \textbf{Completamente observável} vs \textbf{parcialmente observável};
   \item \textbf{Determinístico} vs \textbf{estocástico};
   \item \textbf{Episódico} vs \textbf{sequencial};
   \item \textbf{Estático} vs \textbf{dinâmico};
   \item \textbf{Discreto} vs \textbf{contínuo};
   \item \textbf{Agente único} vs \textbf{multiagente};
   \item \textbf{Cooperativo} vs \textbf{competitivo}.
  \end{itemize}

\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
  \begin{itemize}
   \item Agentes é um conceito unificador e estruturante em IA;
   \item Agentes possuem sensores, atuadores, função de desempenho e estão em
   um ambiente;
   \item Ambientes são os locais onde o agente existe e age, e podem ter
   características que os deixam mais ou menos complexos.
  \end{itemize}
\end{frame}

\begin{frame}{Conclusões}
  \begin{center}
    \includegraphics[scale=0.2]{robot-car.jpeg}
  \end{center}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
