\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Modelagem dos Dados de Entrada}
\subtitle{Simulação Discreta}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
 Nessa aula serão discutidas as maneiras pelas quais dados de entrada em um
 projeto de simulação discreta são obtidos e pré-tratados.
 \newline\newline
 Também serão apresentadas aplicações muito diretas dos conceitos de Estatística
 Descritiva vistos anteriormente.
\end{frame}

\begin{frame}{Introdução}
 No geral, o procedimento de modelagem dos dados compreende três fases:
 \begin{itemize}
  \item Coleta de dados;
  \item Tratamento dos dados;
  \item Inferência
 \end{itemize}
 Essas 3 etapas serão discutidas nessa apresentação.
\end{frame}

\section{Coleta de Dados}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Coleta de Dados}
 A coleta de dados para simulação discreta se inicia com a escolha das variáveis
 de entrada do sistema a ser simulado.
 \newline\newline
 É importante ter muito clara a diferença entre \textbf{dados de entrada} e
 \textbf{dados de saída} -- o que se pretende obter -- da simulação.
\end{frame}

\begin{frame}{Coleta de Dados}
 A maioria dos sistemas que procuramos modelar possui algum fenônemo aleatório
 que o governa. Exemplos:
 \begin{itemize}
  \item Tempo de operação em uma máquina - repetindo a mesma operação, mas
  levando tempos diferentes para processá-la;
  \item Filas de banco - clientes chegam em horários e quantidades diferentes;
  \item Reposição de refil de bebidas em bares - tempo que o consumidor leva
  para consumir uma bebida varia de maneira aleatória.
 \end{itemize}
 Apesar da natureza aleatória dessas variáveis, é possível prever seu
 comportamento probabilístico a partir da observação em tempos anteriores.
\end{frame}

\begin{frame}{Coleta de Dados}
 \begin{block}{Exemplo}
  Um gerente de um supermercado quer estudar o tamanho da fila nos caixas de
  supermercado. Quais variáveis deverão ser analisadas?
  \begin{itemize}
   \item Número de prateleiras no supermercado
   \item Os tempos de atendimento nos caixas
   \item O número de clientes em fila
   \item O tempo de permanência dos clientes no supermercado
   \item O tempo de chegada sucessiva dos clientes nos caixas
  \end{itemize}
 \end{block}
\end{frame}

\begin{frame}{Coleta de Dados}
 \begin{block}{Exemplo}
  Um gerente de um supermercado quer estudar o tamanho da fila nos caixas de
  supermercado. Quais variáveis deverão ser analisadas?
  \begin{itemize}
   \item Número de prateleiras no supermercado
   \item \textbf{Os tempos de atendimento nos caixas}
   \item O número de clientes em fila -- \textbf{Essa medida é resultado!}
   \item O tempo de permanência dos clientes no supermercado
   \item \textbf{O tempo de chegada sucessiva dos clientes nos caixas}
  \end{itemize}
 \end{block}
\end{frame}

\begin{frame}{Coleta de Dados}
 Definidas as variáveis de entrada, cabe agora ao pesquisador mensurar um número
 considerável desses valores com a finalidade de criar uma amostra
 representativa do sistema.
 \newline\newline
 Em que pese essas variáveis serem aleatórias, uma medição correta e precisa de
 uma amostra será suficiente para encontrarmos um modelo probabilístico que rege
 o fenômeno.
\end{frame}

\begin{frame}{Coleta de Dados}
 O trabalho de medição dos dados é um trabalho de campo:
 \begin{itemize}
  \item Ir ao local e, utilizando um cronômetro, medir o tempo em que o fenômeno
  acontece, anotar o valor obtido quando o fenômeno finaliza, zerar o
  cronômetro, fazer nova medição do fenômeno, etc.
  \item O tamanho das observações deve ser entre 100 e 200 amostras. Menor que
  100 pode comprometer a observação do fenômeno probabilístico, enquanto maior
  que 200 não traz ganho significativo.
  \item Coletar e anotar as observações na ordem, para permitir análise de
  correlação.
  \item Deve-se ter clareza se o fenômeno varia conforme o dia, horário ou outra
  variável relacionada com o momento em que foi colhido. Estudos devem levar
  essa característica em conta.
 \end{itemize}
\end{frame}

\begin{frame}{Coleta de Dados}
 Para o estudo da entrada de dados, supomos a medição dos clientes que chegam ao
 supermercado durante um determinado horário. A tabela abaixo apresenta as
 medições, em segundos, dessa entrada, compondo uma amostra de 200 observações.
 {\tiny
 \begin{center}
  \begin{tabular}{cccccccccc}
   11 & 5 & 2 & 0 & 9 & 9 & 1 & 5 & 1 & 3\\
   3 & 3 & 7 & 4 & 12 & 8 & 5 & 2 & 6 & 1\\
   11 & 1 & 2 & 4 & 2 & 1 & 3 & 9 & 0 & 10\\
   3 & 3 & 1 & 5 & 18 & 4 & 22 & 8 & 3 & 0\\
   8 & 9 & 2 & 3 & 12 & 1 & 3 & 1 & 7 & 5\\
   14 & 7 & 7 & 28 & 1 & 3 & 2 & 11 & 13 & 2\\
   0 & 1 & 6 & 12 & 15 & 0 & 6 & 7 & 19 & 1\\
   1 & 9 & 1 & 5 & 3 & 17 & 10 & 15 & 43 & 2\\
   6 & 1 & 13 & 13 & 19 & 10 & 9 & 20 & 19 & 2\\
   27 & 5 & 20 & 5 & 10 & 8 & 2 & 3 & 1 & 1\\
   4 & 3 & 6 & 13 & 10 & 9 & 1 & 1 & 3 & 9\\
   9 & 4 & 0 & 3 & 6 & 3 & 27 & 3 & 18 & 4\\
   6 & 0 & 2 & 2 & 8 & 4 & 5 & 1 & 4 & 18\\
   1 & 0 & 16 & 20 & 2 & 2 & 2 & 12 & 28 & 0\\
   7 & 3 & 18 & 12 & 3 & 2 & 8 & 3 & 19 & 12\\
   5 & 4 & 6 & 0 & 5 & 0 & 3 & 7 & 0 & 8\\
   8 & 12 & 3 & 7 & 1 & 3 & 1 & 3 & 2 & 5\\
   4 & 9 & 4 & 12 & 4 & 11 & 9 & 2 & 0 & 5\\
   8 & 24 & 1 & 5 & 12 & 9 & 17 & 728 & 12 & 6\\
   4 & 3 & 5 & 7 & 4 & 4 & 4 & 11 & 3 & 8
  \end{tabular}
 \end{center}
 }
\end{frame}

\begin{frame}{Coleta de Dados}
 Com os dados medidos, finaliza-se a etapa de coleta dos mesmos e passa-se então
 para o tratamento desses dados.
\end{frame}

\section{Tratamento dos Dados}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Tratamento dos Dados}
 Nessa etapa iniciam-se um conjunto de análises para explorar o conjunto de
 dados e então compreender o fenômeno probabilístico expresso neles.
\end{frame}

\begin{frame}{Tratamento dos Dados}
 Utilizando Estatística Descritiva, obtem-se as seguintes medições para o
 conjunto de dados:
 \begin{center}
  \begin{tabular}{cc}
   \multicolumn{2}{c}{\textbf{Medidas de Centralidade}}\\
   Média & 10,43\\
   Mediana & 5\\
   Moda & 3\\
   Mínimo & 0\\
   Máximo & 728\\
   \multicolumn{2}{c}{\textbf{Medidas de Dispersão}}\\
   Amplitude & 728\\
   Desvio Padrão & 51,41\\
   Variância & 2.643,81\\
   Coeficiente de Variação & 492,74\%\\
   Coeficiente de Assimetria & 0,144
  \end{tabular}
 \end{center}
\end{frame}

\begin{frame}{Tratamento dos Dados}
 Na análise, descobrimos que o mínimo é 0 (diferentes clientes entrando juntos)
 e o máximo é 728 (ou seja, mais de 12 minutos!). A média é 10,44 segundos.
 \newline\newline
 Essa primeira avaliação permite verificar o quão discrepante alguns dados se
 encontram. No caso, aquele 728 é uma medida que deve permanecer no conjunto de
 dados ou ser removida?
 \newline\newline
 Os dados discrepantes são chamados de \textit{outliers} e merecem uma avaliação
 detida sobre o que fazer com eles.
\end{frame}

\begin{frame}{Tratamento dos Dados}
 Antes de discutirmos \textit{outliers}, vale a pena entender o impacto dele nas
 medições do conjunto de dados.
 \newline\newline
 Abaixo temos algumas medições com e sem o valor 728 do conjunto:
 \begin{center}
  \begin{tabular}{ccc}
   \ & \textbf{Com \textit{outlier}} & \textbf{Sem \textit{outlier}}\\
   Média & 10,43 & 6,829\\
   Mediana & 5 & 5\\
   Máximo & 728 & 43\\
   Amplitude & 728 & 43\\
   Desvio Padrão & 51,41 & 6,60\\
   Variância & 2.643,81 & 43,59\\
   Coeficiente de Variação & 492,74\% & 96,68\%\\
  \end{tabular}
 \end{center}
\end{frame}

\subsection{Outlier}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Outlier}
 Dados não usuais em conjuntos de dados são chamados \textit{outliers}.
 \newline\newline
 As razões mais comuns para o seu surgimento são erro na coleta de dados ou a
 ocorrência de um evento raro e inesperado.
\end{frame}

\begin{frame}{Outlier}
 Razões para o surgimento de \textit{outliers}:
 \begin{itemize}
  \item Erro na coleta de dados -- falha em sensor que realiza coleta, mas
  também ocorre quando feita de maneira manual. Erro na atualização da tabela de
  dados, e outras. No geral, quando esse é o motivo do \textit{outlier}, devemos
  remover a observação.
  \item Eventos raros -- um \textit{outlier} difícil de se lidar pois situações
  atípicas podem ocorrer durante as medições.
 \end{itemize}
 Em todo caso, a avaliação se o \textit{outlier} deve ou não ser removido da
 base de dados é uma decisão importante que deve levar em conta o bom senso e
 honestidade sobre o fenômeno em estudo.
\end{frame}

\begin{frame}{Outlier}
 Existem algumas técnicas que permitem uma avaliação se determinado dado é um
 \textit{outlier} a ser removido ou não.
 \newline\newline
 Fazendo Q$_1$ e Q$_3$ os respectivos quartis 1 e 3, temos o cálculo da
 \textbf{Amplitude Interquartil} (A) dada por:
 \begin{center}
  A = Q$_3$ - Q$_1$
 \end{center}
\end{frame}

\begin{frame}{Outlier}
 A partir da Amplitude Interquartil, temos os seguintes conjuntos de valores
 discrepantes:
 \begin{center}
  \begin{tabular}{ccc}
   \textit{Outlier} moderado & valor < Q$_1$ - 1,5A & valor > Q$_3$ + 1,5A\\
   \textit{Outlier} extremo & valor < Q$_1$ - 3A & valor > Q$_3$ + 3A\\
  \end{tabular}
 \end{center}
\end{frame}

\begin{frame}{Outlier}
 Para o exemplo, tem-se:
 \begin{itemize}
  \item Q$_1$ = 2
  \item Q$_3$ = 9
  \item A = Q$_3$ - Q$_1$ = 7 (Amplitude Interquartil)
 \end{itemize}
 \newline
 Assim, os valores discrepantes serão:
 \begin{center}
  \begin{tabular}{ccc}
   \textit{Outlier} moderado & valor < -8,5 & valor > 19,5\\
   \textit{Outlier} extremo & valor < -19 & valor > 30
  \end{tabular}
 \end{center}
\end{frame}

\begin{frame}{Outlier}
 Para o exemplo, teremos 11 valores como \textit{outliers} moderados e 2 como
 \textit{outliers} extremos (43 e 728).
 \newline\newline
 Em nossa avaliação, apenas o valor 728 deveria ser removido, visto que ele
 difere em grande medida dos demais.
 \newline\newline
 Entretanto, cabe o comentário: é importante avaliar bem se o \textit{outlier}
 deve ser removido ou não.
\end{frame}

\subsection{Análise de Correlação}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Análise de Correlação}
 Removido os \textit{outliers}, é necessário ainda fazer uma avaliação se o
 conjunto de dados contém valores independentes ou não.
 \newline\newline
 Essa situação em geral não é válida quando os dados apresentam uma ``curva de
 aprendizado''. Nesses casos, significa que a obtenção dos dados foi realizada
 em alguma situação onde o operador foi ``aprendendo'' a tarefa ao longo do
 tempo, não demonstrando o tempo real de execução da tarefa.
\end{frame}

\begin{frame}{Análise de Correlação}
 Para fazer essa análise, basta criar um gráfico de dispersão com o conjunto de
 dados, na  ordem em que foram auferidos, e verificar se ele está com os pontos
 bem dispersos ou se há alguma aparência de ``evolução'' neles.
 \newline\newline
 Sendo as observações os valores expressos por x$_1$, x$_2$, x$_3$, x$_4$, ...,
 x$_{(n - 1)}$, x$_n$, o gráfico a ser criado deve ser feita pelos pares (x$_1$,
 x$_2$), (x$_3$, x$_4$), ..., (x$_{(n - 1)}$, x$_n$).
\end{frame}

\begin{frame}{Análise de Correlação}
 \begin{figure}
  \includegraphics[scale=0.45]{dispersao-dados}
 \end{figure}
 Acima temos o gráfico de dispersão para o conjunto de dados que trabalhamos no
 exemplo. Ele não aparenta ter alguma evolução.
\end{frame}

\begin{frame}{Análise de Correlação}
 \begin{figure}
  \includegraphics[scale=0.45]{dispersao-evolucao}
 \end{figure}
 No exemplo acima temos um gráfico que apresenta um tipo de ``evolução'', que
 seria a curva de aprendizado.
\end{frame}

\begin{frame}{Análise de Correlação}
 Em Python, podemos utilizar o pacote \textbf{matplotlib.pyplot} para gerar esse
 gráfico:
 \begin{block}{}
  import matplotlib.pyplot as plt\\
  \# sendo x e y as listas com as abscissas e ordenadas do gráfico\\
  plt.plot(x, y, 'o')\\
  plt.show()
 \end{block}
\end{frame}

\section{Inferência}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Inferência}
 Parte-se agora para a última etapa do pré-processamento dos dados, a
 Inferência.
 \newline\newline
 Nessa etapa, tenta-se encontrar o modelo probabilístico que mais se adequa aos
 dados mensurados, de forma que o comportamento das variáveis de entrada seja
 descoberto.
\end{frame}

\subsection{Histograma}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Histograma}
 O primeiro passo da análise para inferência é criar um histograma do conjunto
 de dados.
 \newline\newline
 Um histograma é um gráfico de frequência que mostra como uma população de dados
 está distribuída -- ou seja, ele mostra quantas vezes temos repetido um
 determinado valor ou intervalo de valores no conjunto.
\end{frame}

\begin{frame}{Histograma}
 Para iniciar a análise, precisamos calcular o número de classes a serem
 utilizadas no histograma. A fórmula para esse cálculo é dada por:
 \begin{center}
  K = 1 + 3,3log$_{10}$ n
 \end{center}
 Onde:
 \begin{itemize}
  \item K -- número de classes (portanto, deve ser arredondada para um inteiro)
  \item n -- número de observações na amostra
 \end{itemize}
\end{frame}

\begin{frame}{Histograma}
 Para nosso exemplo, teremos:
 \newline\newline
 K = 1 + 3,3log$_{10}$ n\\
 K = 1 + 3,3log$_{10}$ 199\\
 K = 8,59 $\approx$ 9
 \newline\newline
 Lembrando que como tiramos o \textit{outlier}, agora temos 199 observações.
\end{frame}

\begin{frame}{Histograma}
 Em Python utilizaremos a biblioteca \textbf{math} e a função padrão
 \textbf{round} para realizar esse cálculo da seguinte forma:
 \begin{block}{}
  import math\\
  round(1 + 3.3 * math.log10(199))
 \end{block}
\end{frame}

\begin{frame}{Histograma}
 Com o número de classes sendo igual a 9, procedemos para calcular o tamanho do
 passo para o intervalo de valores em classe. Para tanto, dividimos a amplitude
 pelo número de classes:
 \begin{center}
  h = $\frac{A}{K}$
 \end{center}
 Onde:
 \begin{itemize}
  \item h -- tamanho de cada classe
  \item A -- amplitude
  \item K -- número de classes
 \end{itemize}
\end{frame}

\begin{frame}{Histograma}
 Para o exemplo fica:
 \newline\newline
 h = $\frac{A}{K}$\\
 h = $\frac{43}{9}$\\
 h = 4,8
 \newline\newline
 Ou seja, teremos portanto 9 classes com intervalos de valores com passos de 4,8
 para cada.
\end{frame}

\begin{frame}{Histograma}
 A tabela abaixo apresenta as classes, seus respectivos intervalos de valores, e
 a frequência de cada classe no conjunto de dados.
 \begin{center}
  \begin{tabular}{ccc}
   \textbf{Classes} & \textbf{Intervalos} & \textbf{Frequência}\\
   1 & valor $\leq$ 4,8 & 96\\
   2 & 4,8 < valor $\leq$ 9,6 & 55\\
   3 & 9,6 < valor $\leq$ 14,3 & 25\\
   4 & 14,3 < valor $\leq$ 19,1 & 13\\
   5 & 19,1 < valor $\leq$ 23,9 & 4\\
   6 & 23,9 < valor $\leq$ 28,7 & 5\\
   7 & 28,7 < valor $\leq$ 33,4 & 0\\
   8 & 33,4 < valor $\leq$ 38,2 & 0\\
   9 & 38,2 < valor & 1
  \end{tabular}
 \end{center}
\end{frame}

\begin{frame}{Histograma}
 \begin{figure}
  \includegraphics[scale=0.45]{histograma}
 \end{figure}
 A figura acima apresenta o histograma para o conjunto de dados do exemplo
 trabalhado.
\end{frame}

\begin{frame}{Histograma}
 A exemplo dos gráficos anteriores, também utilizaremos o
 \textbf{matplotlib.pyplot} para gerar o gráfico de histograma. Para tanto,
 precisamos além dos dados, calcular o número de classes e passá-lo como
 argumento na função.
 \begin{block}{}
  import matplotlib.pyplot as plt\\
  \# sendo x o conjunto de dados e k o número de classes\\
  plt.hist(x, bins=k)\\
  plt.show()
 \end{block}
\end{frame}

\subsection{Teste de Aderência}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Teste de Aderência}
 Com o histograma criado, a dúvida que fica é: ``será que os dados podem ser
 modelados a partir de alguma distribuição de probabilidades específica?''
 \newline\newline
 Se isso for possível, o fenômeno modelado pode ser modelado utilizando essa
 função de probabilidade, e ela pode ser utilizada para gerar dados de entrada.
 \newline\newline
 Para verificar se o fenômeno medido é compatível com alguma distribuição,
 fazemos os \textbf{Testes de Aderências}.
\end{frame}

\begin{frame}{Teste de Aderência}
 Os testes de aderência nada mais são do que verificar se o histograma é similar
 a alguma função de probabilidade conhecida.
 \newline\newline
 Há 2 maneiras de realizar essa verificação: a partir de uma análise ``visual''
 do histograma e dos gráficos de distribuições, e a partir de métodos
 matemáticos para esse teste.
\end{frame}

\begin{frame}{Teste de Aderência}
 Sobre o método gráfico, precisamos relembrar as principais funções de
 distribuição probabilidade utilizadas. Veremos as seguintes:
 \begin{itemize}
  \item Uniforme
  \item Exponencial
  \item Normal
  \item Lognormal
  \item Triangular
 \end{itemize}
 Nos slides a seguir, a \textbf{Função} significa a Função Densidade de
 Probabilidade de cada distribuição.
\end{frame}

\begin{frame}{Teste de Aderência -- Distribuição Uniforme}
 \begin{columns}
  \begin{column}{0.6\textwidth}
   \begin{figure}
    \includegraphics[scale=0.45]{uniform}
   \end{figure}
  \end{column}
  \begin{column}{0.4\textwidth}
    \textbf{Parâmetros:}\\
    \textit{a} - menor valor;\\ \textit{b} - maior valor.
    \newline\newline
    \textbf{Função:}
    $\frac{1}{b - a}$
    \newline\newline
    \textbf{Média:}
    $\frac{a + b}{2}$
    \newline\newline
    \textbf{Variância:}
    $\frac{(b - a)^2}{12}$
  \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Teste de Aderência -- Distribuição Exponencial}
 \begin{columns}
  \begin{column}{0.6\textwidth}
   \begin{figure}
    \includegraphics[scale=0.45]{expon}
   \end{figure}
  \end{column}
  \begin{column}{0.4\textwidth}
    \textbf{Parâmetros:}\\
    $\lambda$ - taxa de ocorrências
    \newline\newline
    \textbf{Função:}
    $\lambda e^{-\lambda x}$
    \newline\newline
    \textbf{Média:}
    $\frac{1}{\lambda}$
    \newline\newline
    \textbf{Variância:}
    $\frac{1}{\lambda^2}$
  \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Teste de Aderência -- Distribuição Normal}
 \begin{columns}
  \begin{column}{0.6\textwidth}
   \begin{figure}
    \includegraphics[scale=0.45]{norm}
   \end{figure}
  \end{column}
  \begin{column}{0.4\textwidth}
    \textbf{Parâmetros:}\\
    $\sigma^2$ - variância\\
    $\mu$ - média
    \newline\newline
    \textbf{Função:}
    $\frac{1}{\sigma\sqrt{2\pi}}e^\frac{-(x - \mu)^2}{2\sigma^2}$
    \newline\newline
    \textbf{Média:}
    $\mu$
    \newline\newline
    \textbf{Variância:}
    $\sigma^2$
  \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Teste de Aderência -- Distribuição Lognormal}
 \begin{columns}
  \begin{column}{0.6\textwidth}
   \begin{figure}
    \includegraphics[scale=0.45]{lognorm}
   \end{figure}
  \end{column}
  \begin{column}{0.4\textwidth}
    \textbf{Parâmetros:}\\
    $\sigma$ - forma ou dispersão\\
    $\mu$ - escala ou posição
    \newline\newline
    \textbf{Função:}
    $\frac{1}{\sigmax\sqrt{2\pi}}e^\frac{-(ln(x) - \mu)^2}{2\sigma^2}$
    \newline\newline
    \textbf{Média:}
    $e^{\mu + \frac{\sigma^ 2}{2}}$
    \newline\newline
    \textbf{Variância:}
    $e^{2\mu + \sigma^2}(e^{\sigma^2} )$
  \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Teste de Aderência -- Distribuição Triangular}
 \begin{columns}
  \begin{column}{0.6\textwidth}
   \begin{figure}
    \includegraphics[scale=0.45]{triang}
   \end{figure}
  \end{column}
  \begin{column}{0.4\textwidth}
    \textbf{Parâmetros:}\\
    $a$ - menor valor\\
    $b$ - maior valor\\
    $m$ - moda\\
    Onde: $a < m < b$
    \newline\newline
    \textbf{Função:}\\
    $\frac{2(x - a)}{(m - a)(b - a)}$, para $a \leq x \leq m$\\
    $\frac{2(b - x)}{(b - a)(b - a)}$, para $m \leq x \leq b$
    \newline\newline
    \textbf{Média:}\\
    $\frac{a + m + b}{3}$
    \newline\newline
    \textbf{Variância:}\\
    $\frac{a^2 + m^2 + b^2 - ma - ab - mb}{18}$
  \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Teste de Aderência}
 Conhecida as principais distribuições, o teste de aderência visual consistirá
 portanto de verificar visualmente qual delas consegue melhor representar o
 histograma obtido do fenômeno em medição.
 \newline\newline
 Entretanto, é importante notar que esse tipo de abordagem é sujeito a falhas e
 nem sempre preciso.
\end{frame}

\begin{frame}{Teste de Aderência}
 Para uma maior precisão no teste de aderência é recomendado aplicar algum
 método matemático/estatístico para a verificação.
 \newline\newline
 A literatura traz alguns, e nessa aplicaremos um deles: o \textbf{Teste de
 Kolmogorov-Smirnov} (KS).
 \newline\newline
 Esse teste consiste em observar a distância máxima entre a função acumulada das
 observações e a função acumulada de algum modelo teórico de uma distribuição.
 \newline\newline
 Caso essa distância esteja acima de um limiar, pode-se considerar que a
 distribuição observada é aderente à distribuição teórica comparada.
\end{frame}

\begin{frame}{Teste de Aderência}
 Para realizar o teste KS é necessário criar uma tabela com as seguintes
 colunas:
 \newline
 \begin{enumerate}
  \item \textbf{Valor observado} -- identificar quais são os valores presentes
  na base de dados e ordená-los em ordem crescente;
  \item \textbf{Frequência observada} -- contabilizar quantas vezes cada valor
  aparece na base de dados;
  \item \textbf{Frequência acumulada observada} -- somar as frequências
  observadas a cada linha, tanto do valor na linha quanto dos valores menores;
  \item \textbf{Frequência acumulada observada normalizada} -- divide-se a
  frequência acumulada observada pelo número total de dados;
 \end{enumerate}
 \
 \newline
 Ao final dessa primeira parte teremos calculado a frequência de elementos
 observados na base e também a frequência normalizada.
\end{frame}

\begin{frame}{Teste de Aderência}
 Utilizando o exemplo da aula, teríamos as seguintes primeiras linhas na tabela:
 \begin{center}
 \begin{tabular}{ccccccccc}
    \textbf{V.O.} & \textbf{F.O.} & \textbf{F.A.O.} & \textbf{F.A.O.N.}\\
    0 & 13 & 13 & 0.07\\
    1 & 23 & 36 & 0.18\\
    2 & 18 & 54 & 0.27\\
    3 & 26 & 80 & 0.4\\
    ... & ... & ... & ...
 \end{tabular}
 \end{center}
\end{frame}

\begin{frame}{Teste de Aderência}
 No próximo passo, precisaremos adicionar 2 colunas:
 \begin{itemize}
  \item Uma coluna com a frequência acumulada esperada da distribuição teórica
  que iremos comparar com a frequência acumulada observada normalizada;
  \item Uma coluna com o módulo (valor absoluto) entre as frequências acumuladas
  e teóricas calculadas.
 \end{itemize}
 Para a primeira coluna, será necessário a utilização da fórmula matemática da
 distribuição teórica que iremos analisar.
\end{frame}

\begin{frame}{Teste de Aderência}
 Para esse exemplo na tabela, decidiu-se comparar com a distribuição
 exponencial.
 \newline\newline
 Para fazer o cálculo das frequências teóricas, é necessário conhecer a função
 acumulada da distribuição. Para a distribuição exponencial, essa função é dada
 por $1 - e^{-\lambda x}$, onde $\lambda$ é o inverso da média da distribuição.
 \newline\newline
 Calculando a média da distribuição (6,83) teremos $\lambda = 0,146$.
 Assim, a fórmula será $1 - e^{-0,146 x}$.
\end{frame}

\begin{frame}{Teste de Aderência}
 Esse passo pode ser mais simples utilizando um software de planilha ou
 linguagem de programação.
 \newline\newline
 A tabela ficará assim (F.A.T.N. = Frequência Acumulada Teórica Normalizada):
 \begin{center}
 \begin{tabular}{ccccccccc}
    \textbf{V.O.} & \textbf{F.O.} & \textbf{F.A.O.} & \textbf{F.A.O.N.} & \textbf{F.A.T.N.}\\
    0 & 13 & 13 & 0.07 & 0\\
    1 & 23 & 36 & 0.18 & 0.14\\
    2 & 18 & 54 & 0.27 & 0.25\\
    3 & 26 & 80 & 0.4 & 0.35\\
    ... & ... & ... & ... & ...
 \end{tabular}
 \end{center}
\end{frame}

\begin{frame}{Teste de Aderência}
 Calculado o F.A.T.N., agora criaremos uma nova coluna com o módulo (valor
 absoluto) da diferença entre F.A.T.N. e F.A.O.N., que chamaremos \textbf{D}.
 \newline\newline
 A tabela ficará conforme:
 \begin{center}
 \begin{tabular}{ccccccccc}
    \textbf{V.O.} & \textbf{F.O.} & \textbf{F.A.O.} & \textbf{F.A.O.N.} & \textbf{F.A.T.N.} & \textbf{D}\\
    0 & 13 & 13 & 0.07 & 0 & 0.07\\
    1 & 23 & 36 & 0.18 & 0.14 & 0.04\\
    2 & 18 & 54 & 0.27 & 0.25 & 0.02\\
    3 & 26 & 80 & 0.4 & 0.35 & 0.05\\
    ... & ... & ... & ... & ... & ...
 \end{tabular}
 \end{center}
 Finalmente, para verificar o teste de aderência, utilizamos o maior valor de
 \textbf{D} e verificamos a tabela do KS para saber como calcular o valor
 D$_{crítico}$ dado um nível de significância $\alpha$.
\end{frame}

\begin{frame}{Teste de Aderência}
 Na tabela do KS o valor a ser buscado é relacionado com o $\alpha$ e o número
 de observações. Fazendo $\alpha = 0.05$ e lembrando que temos ($n =$) 199
 observações, a tabela nos indicará que o valor D$_{crítico}$ deve ser calculado
 a partir da fórmula $\frac{1,36}{\sqrt{n}}$.
 \newline\newline
 Assim: D$_{crítico} = \frac{1,36}{\sqrt{199}} = 0,0964$.
 \newline\newline
 Como esse valor é maior que o maior valor de \textbf{D}, então a distribuição
 é aderente ao conjunto de dados.
\end{frame}

\begin{frame}{Teste de Aderência}
 \begin{alertblock}{Atenção}
  A Tabela do KS comentada no slide anterior está disponível como material
  complementar da aula.
 \end{alertblock}
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
\begin{itemize}
 \item Tratamento de dados em simulação discreta é essencial para realização das
 simulações;
 \item Os dados precisam ser medidos em campo ou coletados a partir de tabelas,
 e em seguida é necessário encontrar uma distribuição probabilística que modele
 de que maneira o fenômeno se comporta;
 \item Com os histogramas e o teste de aderência via KS, é possível verificar se
 determinados dados é condizente com alguma distribuição probabilística.
\end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
