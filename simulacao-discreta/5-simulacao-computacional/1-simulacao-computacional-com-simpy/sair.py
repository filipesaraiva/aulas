class Sair:
    def __init__(self, env, t_video_game, t_ligacao):
        self.env = env
        self.ev_video_game = self.env.event()
        self.ev_ligacao = self.env.event()
        self.env.process(self.decide_sair())
        self.env.process(self.video_game(t_video_game))
        self.env.process(self.ligacao(t_ligacao))

    def decide_sair(self):
        yield self.ev_video_game | self.ev_ligacao
        print('Saio em', self.env.now)

    def video_game(self, t_video_game):
        yield self.env.timeout(t_video_game)
        self.ev_video_game.succeed()

    def ligacao(self, t_ligacao):
        yield self.env.timeout(t_ligacao)
        self.ev_ligacao.succeed()

import simpy
env = simpy.Environment()

o = Sair(env, 13, 5)

env.run()
