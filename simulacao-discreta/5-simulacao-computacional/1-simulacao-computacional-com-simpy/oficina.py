class Oficina:
    def __init__(self, env):
        self.env = env
        self.mecanico_disponivel = self.env.event()
        self.env.process(self.chegou_carro())
        self.env.process(self.chegou_mecanico())

    def chegou_carro(self):
        yield self.mecanico_disponivel
        print('Desmonte do carro em', self.env.now)

    def chegou_mecanico(self):
        yield self.env.timeout(13)
        self.mecanico_disponivel.succeed()

import simpy
env = simpy.Environment()

o = Oficina(env)

env.run()
