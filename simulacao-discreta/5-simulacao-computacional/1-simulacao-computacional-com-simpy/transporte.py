class Carro:
    def __init__(self, env):
        self.frequencia = 3
        self.env = env
        self.env.process(self.run())

    def run(self):
        while True:
            yield self.env.timeout(self.frequencia)
            print('Carro passou em', self.env.now)

class Onibus:
    def __init__(self, env):
        self.frequencia = 5
        self.env = env
        self.env.process(self.run())

    def run(self):
        while True:
            yield self.env.timeout(self.frequencia)
            print('Ônibus passou em', self.env.now)

class Moto:
    def __init__(self, env):
        self.frequencia = 2
        self.env = env
        self.env.process(self.run())

    def run(self):
        while True:
            yield self.env.timeout(self.frequencia)
            print('Moto passou em', self.env.now)
