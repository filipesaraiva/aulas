class Carro:
    def __init__(self, env, nome, tempo_chegada, tempo_abastecimento):
        self.env = env
        self.nome = nome
        self.tempo_chegada = tempo_chegada
        self.tempo_abastecimento = tempo_abastecimento

class Posto:
    def __init__(self, env, bombas):
        self.env = env
        self.bombas = bombas
        
    def abastecer(self, carro):
        yield self.env.timeout(carro.tempo_chegada)
        print(carro.nome, 'chegou em', self.env.now)
        with self.bombas.request() as bomba:
            yield bomba
            print(carro.nome, 'iniciou abastecimento em', self.env.now)
            yield self.env.timeout(carro.tempo_abastecimento)
            print(carro.nome, 'saiu do abastecimento em', self.env.now)

import simpy
env = simpy.Environment()

c1 = Carro(env, 'Lada', 5, 3)        
c2 = Carro(env, 'Fusca', 0, 3)     
c3 = Carro(env, 'Opala', 4, 2)       
c4 = Carro(env, 'Fiat 147', 8, 2)       
c5 = Carro(env, 'Santana', 12, 8)          

bombas = simpy.Resource(env, capacity=1)
p = Posto(env, bombas)

env.process(p.abastecer(c1))
env.process(p.abastecer(c2))
env.process(p.abastecer(c3))
env.process(p.abastecer(c4))
env.process(p.abastecer(c5))

env.run()
