def fila(env, recurso, pessoa, chegada, prioridade):
    yield env.timeout(chegada)
    with recurso.request(priority=prioridade) as req:
        yield req
        print(pessoa, 'atendida em', env.now)
        yield env.timeout(5)

import simpy

env = simpy.Environment()
recurso = simpy.PriorityResource(env, capacity=1)

env.process(fila(env, recurso, 'Maria', 0, 2))
env.process(fila(env, recurso, 'Paula', 1, 2))
env.process(fila(env, recurso, 'Alana', 2, 0))

env.run()
        
