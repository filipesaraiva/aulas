class Pessoa:
    def __init__(self, env):
        self.env = env
        self.env.process(self.run())
    
    def run(self):
        while True:
            print('Começou a estudar em', self.env.now)
            tempo_estudo = 2
            yield self.env.timeout(tempo_estudo)
            
            print('Foi para as redes sociais em', self.env.now)
            tempo_redes_sociais = 5
            yield self.env.timeout(tempo_redes_sociais)
