def fila(env, recurso, pessoa, chegada):
    yield env.timeout(chegada)
    with recurso.request() as req:
        yield req
        print(pessoa, 'atendida em', env.now)
        yield env.timeout(5)

import simpy
import numpy
import scipy.stats as st

env = simpy.Environment()
recurso = simpy.Resource(env, capacity=1)

numpy.random.seed(51)
p = st.expon.rvs(size=10, loc=0, scale=3)

for i in range(len(p)):
    env.process(fila(env, recurso, i, p[i]))

env.run()
        
