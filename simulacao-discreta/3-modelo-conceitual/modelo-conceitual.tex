\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

\lstset{showstringspaces=false}
%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Modelo Conceitual}
\subtitle{Simulação Discreta}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Introdução}
 Para se criar um modelo para simulação é necessário idealizá-lo a partir de
 \textbf{abstrações} de seus componentes e ações.
\end{frame}

\begin{frame}{Introdução}
 Um modelo de simulação deve ser simples em seus detalhes mas ao mesmo tempo
 codificar os principais acontecimentos do sistema real, de forma que ele sirva
 para os propósitos de avaliação do real.
\end{frame}

\begin{frame}{Introdução}
 Modelar pressupõe um balanceamento sobre o detalhamento afim de se criar bons
 modelos.
 \begin{itemize}
  \item Quanto maior detalhamento, mais complexo fica o sistema e mais custoso
  será a realização das simulações, tornando no extremo impossível de
  realizá-las;
  \item Quanto menor o detalhamento, mais simples fica o sistema mas ao mesmo
  tempo as simulações ficam distantes do que seria o funcionamento do sistema
  no mundo real.
 \end{itemize}
\end{frame}

\begin{frame}{Introdução}
 Nessa aula dissertaremos sobre as diretrizes para se criar bons modelos para
 utilização em simulações além de técnicas de especificação empregadas.
\end{frame}

\section{Abstração}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Abstração}
 \begin{block}{Abstração}
  Em termos de modelagem para simulação, \textbf{abstração} é a capacidade de
  identificar o que é realmente importante no sistema para trazê-lo para o
  modelo.
 \end{block}
\end{frame}

\begin{frame}{Abstração}
 \textbf{Exemplo}
 \newline\newline
 Supondo que busca-se simular a produtividade com que trabalhadores de uma linha
 de produção desempenham suas funções, quais desses dados deveriam ser
 descartados do modelo:
 \begin{itemize}
  \item O tempo que o trabalhador leva no deslocamento de casa para o trabalho;
  \item O tempo que o trabalhador realiza a primeira tarefa do dia;
  \item O material utilizado no piso da fábrica;
  \item A velocidade com que uma máquina processa uma tarefa ordenada pelo
  trabalhador.
 \end{itemize}
\end{frame}

\begin{frame}{Abstração}
 \textbf{Exemplo}
 \newline\newline
 Supondo que busca-se simular a produtividade com que trabalhadores de uma linha
 de produção desempenham suas funções, quais desses dados deveriam ser
 descartados do modelo:
 \begin{itemize}
  \item \textbf{O tempo que o trabalhador leva no deslocamento de casa para o
  trabalho;}
  \item O tempo que o trabalhador realiza a primeira tarefa do dia;
  \item \textbf{O material utilizado no piso da fábrica;}
  \item A velocidade com que uma máquina processa uma tarefa ordenada pelo
  trabalhador.
 \end{itemize}
\end{frame}

\begin{frame}{Abstração}
 \begin{alertblock}{Atenção}
  Algumas variáveis tem impacto na simulação em modelo, mas são complexas demais
  para serem modeladas matematicamente.
  \newline\newline
  A utilização do bom-senso e saber entender a natureza desses fenômenos e como
  resolvê-los é mais importante e efetivo do que modelá-los matematicamente.
 \end{alertblock}
\end{frame}

\begin{frame}{Abstração}
 \textbf{Exemplo}
 \newline\newline
 Em um posto de gasolina, uma série de carros chegam para abastecer ao longo de
 determinado período. Busca-se modelar esse sistema de forma a aumentar o número
 de atendimentos. Quais desses dados deveriam ser descartados do modelo:
 \begin{itemize}
  \item O tamanho do tanque de combustível de cada carro;
  \item O tempo que o carro passa abastecendo no posto;
  \item O tempo de chegada dos carros no posto;
  \item A velocidade com a qual o carro sai do posto;
  \item A velocidade de atendimento dos frentistas.
 \end{itemize}
\end{frame}

\begin{frame}{Abstração}
 \textbf{Exemplo}
 \newline\newline
 Em um posto de gasolina, uma série de carros chegam para abastecer ao longo de
 determinado período. Busca-se modelar esse sistema de forma a aumentar o número
 de atendimentos. Quais desses dados deveriam ser descartados do modelo:
 \begin{itemize}
  \item \textbf{O tamanho do tanque de combustível de cada carro;}
  \item O tempo que o carro passa abastecendo no posto;
  \item O tempo de chegada dos carros no posto;
  \item \textbf{A velocidade com a qual o carro sai do posto;}
  \item \textbf{A velocidade de atendimento dos frentistas.}
 \end{itemize}
\end{frame}

\begin{frame}{Abstração}
 O modelador deve observar com muito cuidado quais variáveis de fato influem no
 sistema para o tipo de estudo que está sendo realizado, de forma que ele
 consiga selecionar quais as que precisam e as que não precisam ser levadas em
 conta.
\end{frame}

\section{Modelo Conceitual ACD}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 Existem várias técnicas para desenvolver conceitualmente um modelo de simulação
 discreta.
 \newline\newline
 Nessa apresentação veremos o \textbf{ACD} -- \textit{Activity Cicle Diagram},
 que permite uma visualização simples das entidades, processos e ordenamento do
 processamento dos mesmos em um modelo de simulação discreta.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 O ACD é muito voltado para simulações cuja característica é a presença de
 filas, muito comuns em simulação discreta.
 \newline\newline
 No geral em um ACD descrevemos as \textbf{Entidades} do sistema, que são
 aquelas que abstraímos e modelamos anteriormente, e os estados que essas
 entidades poderão assumir.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 No ACD existem 2 tipos de representações gráficas para descrever os estados,
 que nada mais são que o ciclo de vida de entidades e objetos do sistema:
 \begin{itemize}
  \item \textbf{Círculos:} representam filas;
  \item \textbf{Retângulos:} representam atividades;
 \end{itemize}
 É importante ressaltar que \textbf{cada Entidade tem seu próprio ciclo de
 vida}, tendo portanto suas próprias filas e atividades.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 Abaixo temos as duas maneiras de representar o ciclo de vida das entidades:
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.3]{fila-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.3]{atividade-ACD.eps}
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.3]{fila-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     \textbf{Fila}
     \newline\newline
     Diz-se que as entidades estão em \textbf{Fila} quando elas estão esperando
     que algo aconteça para iniciar alguma atividade. Esse é o estado onde as
     entidades estão em \textit{stand by}.
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.3]{atividade-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     \textbf{Atividade}
     \newline\newline
     Uma \textbf{Atividade}, em geral, exige a participação de diferentes
     entidades e demanda um certo tempo de processamento para executar.
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Imagine que um porto com um determinado número de pontos de ancoragem serve
 para descarregar e carregar navios. Identifique:
 \begin{itemize}
  \item Entidades;
  \item Filas;
  \item Atividades.
 \end{itemize}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Há várias maneiras de resolver esse exemplo, tudo dependendo do nível de
 detalhamento que se busca: é possível contar com máquinas de carga/descarga,
 com práticos (que auxiliam no ancoragem/desancoragem dos navios), e mais.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Modelando apenas navios, portos de ancoragem e máquinas de carga/descarga:
 \begin{itemize}
  \item \textbf{Entidades}: navios, máquinas, pontos de carga/descarga;
  \item \tetbf{Filas}: fila de atracagem no porto; fila de utilização de
  máquinas;
  \item \textbf{Atividades}: atracar o navio; carregar/descarregar o navio;
 \end{itemize}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Vamos agora criar o modelo conceitual ACD para cada entidade modelada.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Vamos agora criar o modelo conceitual ACD para cada entidade modelada.
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.3]{navio-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     Ao chegar na região portuária, o navio precisa esperar que algum ponto de
     atracagem esteja disponível.
     \newline\newline
     Se disponível, ele atraca e realiza o procedimento de carregar ou
     descarregar.
     \newline\newline
     Após finalizada a operação, ele sai do sistema.
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Vamos agora criar o modelo conceitual ACD para cada entidade modelada.
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.25]{portos-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     Os pontos de ancoragem são outras entidades do sistema. Se livres, elas
     podem ser utilizadas para a ancoragem.
     \newline\newline
     Após o navio realizar a operação desejada e partir, o ponto de ancoragem se
     encontrará livre novamente.
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Vamos agora criar o modelo conceitual ACD para cada entidade modelada.
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.25]{maquinas-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     Para realizar as operações, o navio necessita de máquinas para que operem
     as cargas e descargas.
     \newline\newline
     Se disponível, eles podem realizar a operação. Quando finalizada, eles
     retornam para a fila de livre.
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Modelada cada entidade separadamente e suas atividades, é chegada a hora de
 integrá-las no modelo total do sistema de simulação.
 \newline\newline
 Para tanto, deve-se buscar as atividades que demandam mais de uma entidade --
 elas são as responsáveis pela integração.
 \newline\newline
 Por exemplo, para realizar a carga e a descarga são necessários 3 requisitos:
 que haja um navio; que um ponto de atracagem esteja disponível; que hajam
 máquinas disponíveis.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Sistema integrado para o exemplo.
 {\centering
 \includegraphics[scale=0.3]{integracao1-ACD.eps}}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Entretanto, revisando o modelo integrado, percebemos que o navio só irá atracar
 quando tivermos máquinas também disponíveis, o que não faz muito sentido.
 \newline\newline
 Assim, partiremos para revisar essa entidade.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Vamos agora criar o modelo conceitual ACD para cada entidade modelada.
 \begin{columns}
    \begin{column}{0.5\textwidth}
     \centering
     \includegraphics[scale=0.2]{navio-revisado-ACD.eps}
    \end{column}
    \begin{column}{0.5\textwidth}
     Ao chegar na região portuária, o navio precisa esperar que algum ponto de
     atracagem esteja disponível.
     \newline
     Se disponível, ele atraca.
     \newline
     Após, aguarda máquinas para o procedimento de carregar ou
     descarregar.
     \newline
     Quando disponíveis, carrega ou descarrega.
     \newline
     Após finalizada a operação, ele sai do sistema.
    \end{column}
 \end{columns}
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Perceba que agora há uma modificação no sistema integrado revisado: nas
 atividades dos pontos de carga e descarga, após serem utilizados para atracagem
 do navio, só estarão livres novamente após finalizada a atividade de carga e
 descarga.
 \newline\newline
 É possível adicionar uma fila no diagrama específico dos pontos de carga/
 descarga, para facilitar a interpretação das atividades.
\end{frame}

\begin{frame}{Modelo Conceitual ACD}
 \textbf{Exemplo}
 \newline\newline
 Sistema integrado e revisado para o exemplo.
 {\centering
 \includegraphics[scale=0.25]{integracao2-ACD.eps}}
\end{frame}

\section{Simulando sobre o ACD}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Simulando sobre o ACD}
 Com o modelo ACD fica mais fácil visualizar o fluxo das entidades e suas
 relações para a simulação.
 \newline\newline
 Mas, para além dessa aplicação, é possível fazer simulações sobre o modelo e
 verificar como ele se comporta.
 \newline\newline
 Para realizar essa espécie de ``simulação manual'' estudaremos o método chamado
 Método as Três Fases.
\end{frame}

\begin{frame}{Simulando sobre o ACD}
 Cabe lembrar quê:
 \begin{itemize}
  \item Cada atividade no modelo de simulação pressupõe o processamento durante
  um certo intervalo de tempo;
  \item Os tempos de processamento são medidos em campo pelos pesquisadores e
  modelados segundo alguma distribuição probabilística (vide conteúdo anterior);
  \item Com a distribuição probabilística encontrada, geram-se dados de entrada
  para as entidades seguindo essa distribuição.
 \end{itemize}
 Para o modelo de simulação manual, utilizaremos apenas medições fixas para
 facilitar o entendimento -- mas que fique claro que a mesma tem apenas uma
 utilização didática, e não serviria para simular sistemas reais.
\end{frame}

\begin{frame}{Método das 3 Fases}
 O método das 3 fases consiste em criar uma tabela que vai acompanhando a linha
 de tempo de uma simulação e as ações que ocorrem nela.
 \newline\newline
 As fases consistem em:
 \begin{itemize}
  \item \textbf{Fase A:} verifique o tempo de término para todas as atividades
  em progresso. Encontre a que terminará antes. Avance o relógio de simulação
  para esse instante;
  \item \textbf{Fase B:} para as atividades finalizadas, mova-as para suas
  respectivas filas;
  \item \textbf{Fase C:} inicie atividades que estão livres para começar e mova
  as entidades apropriadas das filas para as atividades. Anote o tempo de
  término dessa atividade.
 \end{itemize}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Na tabela abaixo tem-se as fases que devem ser preenchidas no decorrer da
 simulação.
  \begin{table}[]
   \begin{tabular}{|c|c|c|}
     \hline
     \textbf{A} & \textbf{B} & \textbf{C} \\ \hline
     × & × & × \\ \hline
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Para ilustrar o método, será utilizado o exemplo revisado da simulação de
 carga/descarga em um porto, utilizando 3 navios, 2 pontos de carga/descarga, e
 2 máquinas para carga/descarga.
 \newline\newline
 Para simplificar, teremos que os navios passam 3 horas para atracar, e 6 horas
 para carregar/descarregar.
 \newline\newline
 Essas medidas, no geral, deveriam ser estabelecidas a partir de uma
 distribuição probabilística.
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Sistema com as entidades para o Tempo 0
 {\centering
 \includegraphics[scale=0.25]{integracao2-ACD-ex1.eps}}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Com o tempo 0, as fases a serem verificadas serão a A (para marcar o tempo da
 simulação) e a C (para verificar quais atividades podem ser executadas).
 \newline\newline
 Do diagrama, temos que há navios e portos disponíveis. Logo, a atividade
 ``Atraca'' pode ser iniciada. Cada navio levará 3 horas para atracar. Portanto,
 a tabela ficará da seguinte forma:
  \begin{table}[]
   \begin{tabular}{|c|c|c|}
     \hline
     \textbf{A} & \textbf{B} & \textbf{C} \\ \hline
     0 & × & 2 Atracas começam e terminam em 3 \\ \hline
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Sistema com as entidades para o Tempo 3
 {\centering
 \includegraphics[scale=0.25]{integracao2-ACD-ex2.eps}}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Com as atracagens finalizadas, o relógio em A muda para o Tempo 3 (primeira
 finalização de atividade anterior), B envia a combinação navio + porto para a
 fila de espera de carga/descarga, e em C, como temos máquinas disponíveis,
 podemos iniciar em paralelo as atividades de carga/descarga.
  \begin{table}[]
   \begin{tabular}{|c|c|c|}
     \hline
     \textbf{A} & \textbf{B} & \textbf{C} \\ \hline
     0 & × & 2 Atracas começam e terminam em 3 \\ \hline
     3 & 2 Atracas terminam & 2 Cargas começam e terminam em 9 \\ \hline
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Sistema com as entidades para o Tempo 9
 {\centering
 \includegraphics[scale=0.25]{integracao2-ACD-ex3.eps}}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Finalizado as operações de carga/descarga, todas as entidades estão livres para
 seguir seus destinos. No caso, os 2 navios se vão, e as duas máquinas e portos
 voltam para suas filas de livres. Entretanto, com o porto disponível, já é
 possível que o próximo navio atraque.
  \begin{table}[]
   \begin{tabular}{|c|c|c|}
     \hline
     \textbf{A} & \textbf{B} & \textbf{C} \\ \hline
     0 & × & 2 Atracas começam e terminam em 3 \\ \hline
     3 & 2 Atracas terminam & 2 Cargas começam e terminam em 9 \\ \hline
     9 & 2 Cargas terminam & 1 Atracar começa e termina em 12 \\ \hline
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Sistema com as entidades para o Tempo 12
 {\centering
 \includegraphics[scale=0.25]{integracao2-ACD-ex4.eps}}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Finalizada a atracagem do último navio, como há máquinas disponíveis, ele segue
 para a operação de carga/descarga nos tempos conforme a tabela.
  \begin{table}[]
   \begin{tabular}{|c|c|c|}
     \hline
     \textbf{A} & \textbf{B} & \textbf{C} \\ \hline
     0 & × & 2 Atracas começam e terminam em 3 \\ \hline
     3 & 2 Atracas terminam & 2 Cargas começam e terminam em 9 \\ \hline
     9 & 2 Cargas terminam & 1 Atracar começa e termina em 12 \\ \hline
     12 & Atracar termina & Carga começa e termina em 18 \\ \hline
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Sistema com as entidades para o Tempo 18
 {\centering
 \includegraphics[scale=0.25]{integracao2-ACD-ex5.eps}}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 E finalmente, a última operação termina em 18, finalizando as operações com os
 navios e liberando as demais entidades.
  \begin{table}[]
   \begin{tabular}{|c|c|c|}
     \hline
     \textbf{A} & \textbf{B} & \textbf{C} \\ \hline
     0 & × & 2 Atracas começam e terminam em 3 \\ \hline
     3 & 2 Atracas terminam & 2 Cargas começam e terminam em 9 \\ \hline
     9 & 2 Cargas terminam & 1 Atracar começa e termina em 12 \\ \hline
     12 & Atracar termina & Carga começa e termina em 18 \\ \hline
     18 & Carga termina & \\ \hline
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}{Método das 3 Fases}
 \textbf{Exemplo}
 \newline\newline
 Após o Tempo 18, a simulação do sistema para essas entradas é finalizada.
 \newline\newline
 É importante notar que o relógio de simulação da Fase A sempre deve ser
 configurado para a atividade descrita na Fase C que finaliza mais cedo, para
 que seja permitido o acompanhamento da simulação em detalhes.
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
\begin{itemize}
 \item Modelos conceituais são importantes para verificar como a simulação se
 comportaria antes de realizar a implementação da mesma;
 \item Para o modelo conceitual é necessário abstrair as entidades e o fluxo de
 atividades de cada uma. Em seguida, verificar quais dessas atividades demanda a
 participação de quais e quantas entidades;
 \item É possível simular o modelo conceitual ACD a partir da simulação de
 ações realizadas pelas entidades utilizando o método das 3 fases.
\end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
