\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}

%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in
%   itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to
%   black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this
%% command: 
\setbeamercolor{structure}{fg=ETHred}

%% Mandatory variables
\author{Filipe Saraiva}
\title{Método Simplex}
\subtitle{Matemática Computacional I}
\institute{UFPA}
\date{\today}

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Conteúdo}
	\tableofcontents
\end{frame}

\section{Introdução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conceitos Fundamentais}
Antes de iniciarmos os estudos do Método Simplex, é importante conhecermos
alguns conceitos fundamentais que embasam a aplicação do método. São eles:
\begin{itemize}
 \item Forma Canônica de um PPL
 \item Variáveis Básicas
\end{itemize}
\end{frame}

\subsection{Forma Padrão e Forma Canônica}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Forma Padrão}
Quando modelamos um problema de otimização encontramos a Função Objetivo e
diversas funções de Restrições dos tipos $\leq, = \text{ou} \geq$, que formam um
sistema de inequações.
\newline\newline
A essa etapa da modelagem encontramos o que a área chama de Forma Padrão do
problema.
\end{frame}

\begin{frame}{Forma Padrão}
    Dizemos que o PPL está na Forma Padrão se ele apresenta a seguinte forma:\\
    \begin{center}
    $\text{Min Z} = c^Tx + d$
    \newline Suj. a: $\begin{cases}
    Ax = b\\
    x \geq 0
    \end{cases}$
    \end{center}
    Ou seja, quando a Função Objetivo é de minimização e as restrições estão
    todas na forma de igualdade ($Ax = b$).
\end{frame}

\begin{frame}{Forma Padrão}
    Para conseguirmos encontrar a Forma Padrão de um PPL, devemos realizar
    certas transformações tanto na Função Objetivo quanto no conjunto de
    Restrições do problema.
    \newline\newline
    Na Forma Padrão os problemas devem ser transformados para o tipo
    Minimização. Problemas de Minimização já estão corretos, enquanto problemas
    de Maximização são transformados de acordo com o esquema abaixo. Veja o
    exemplo:
    \begin{center}
        Max Z = - Min(-Z)\\
        $\text{Max} (8 X_1 + 6 X_2)$ = - $\text{Min} (-8 X_1 - 6 X_2)$
    \end{center}
\end{frame}

\begin{frame}{Forma Padrão}
    Para o conjunto de restrições, devemos utilizar as chamadas Variáveis de
    Folga, que transformam as inequações em equações.
    \newline\newline
    A explicação do uso dessas variáveis é a seguinte: se uma restrição é do 
    tipo $\leq$, significa que existe um valor que somado a esta equação
    garantirá que a mesma será $=$.
\end{frame}

\begin{frame}{Forma Padrão}
    Portanto, dado uma restrição por exemplo:
    \begin{center}
     $2 X_1 + 3 X_2 + X_3 + 1 \leq 12$
    \end{center}
    Poderíamos utilizar uma variável de folga, por exemplo $X_4$, para torná-
    la uma equação, ficando então:
    \begin{center}
     $2 X_1 + 3 X_2 + X_3 + 1 + X_4 = 12$
    \end{center}
    Conceito similar deve ser empregado para restrições do tipo $\geq$, onde
    nesse caso utilizaremos variáveis de folga negativas ($- X_4$).
\end{frame}

\begin{frame}{Forma Padrão}
    Mesmo para restrições de igualdade, é importante utilizar Variáveis de Folga
    pois o método de resolução Simplex utilizará essas variáveis em seus
    processos.
    \newline\newline
    Nesses casos, adiciona-se a expressão $X_i - X_i$ para indicar a
    utilização dessas variáveis. Portanto, o exemplo abaixo:
    \begin{center}
     $X_1 + X_2 + 2 X_3 = 12$
    \end{center}
    Ficaria:
    \begin{center}
     $X_1 + X_2 + 2 X_3 + X_4 - X_4 = 12$
    \end{center}
\end{frame}

\begin{frame}{Forma Padrão}
    Exemplo: transforme o seguinte PPL para a Forma Padrão:
    \begin{center}
    $\text{Max Z} = 8.000 X_1 + 6.000 X_2$
    \newline Suj. a: $\begin{cases}
    X_1 \leq 200\\
    X_1 \geq X_2\\
    X_1 + X_2 \leq 350\\
    4X_1 + 3X_2 \leq 1400\\
    X_1, X_2 \geq 0
    \end{cases}$
    \end{center}
\end{frame}

\begin{frame}{Forma Padrão}
    Exemplo: transforme o seguinte PPL para a Forma Padrão [\textbf{Resposta}]:
    \begin{center}
    $\text{Min Z} = -8.000 X_1 - 6.000 X_2$
    \newline Suj. a: $\begin{cases}
    X_1 + X_3 = 200\\
    X_1 - X_2 - X_4 = 0\\
    X_1 + X_2 + X_5 = 350\\
    4X_1 + 3X_2 + X_6 = 1400\\
    X_1, X_2, X_3, X_4, X_5, X_6 \geq 0
    \end{cases}$
    \end{center}
\end{frame}

\begin{frame}{Forma Canônica}
    Para o caso em que o modelo de PPL está na Forma Padrão e é possível
    encontrar nas restrições uma submatriz quadrada identidade ($A_S = I$), com
    valor 0 nos respectivos coeficientes na Função Objetivo, dizemos que o
    modelo PPL está na Forma Canônica. Por exemplo:
    \newline\newline
    \begin{center}
    $\text{Min Z} = 2X_1 + X_3 - X_5 + X_6$
    \newline Suj. a: $\begin{cases}
    3X_1 - 2X_3 + X_5 + X_7 = 1\\
    2X_1 + X_2 - 3X_5 = 8\\
    3X_1 + 17X_3 + X_4 + X_6 = 2\\
    X_i \geq 0, \forall i = 1, 2, ..., 7
    \end{cases}$
    \end{center}
\end{frame}

\begin{frame}{Forma Canônica}
    Colocando o exemplo anterior na forma matricial, teremos:
    \newline\newline
\begin{table}[]
    \centering
    \begin{tabular}{llllllllll}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & $X_5$ & $X_6$ & $X_7$ & \ & \ \\
Min Z & 2 & \textbf{0} & 1 & \textbf{0} & -1 & 1 & \textbf{0} & \ & \\\
\ & 3 & \textbf{0} & -2 & \textbf{0} & 1 & 0 & \textbf{1} & = & 1\\
Suj. a: & 2 & \textbf{1} & 0 & \textbf{0} & -3 & 0 & \textbf{0} & = & 8\\
\ & 3 & \textbf{0} & 17 & \textbf{1} & 0 & 1 & \textbf{0} & = & 2
    \end{tabular}
\end{table}
    Destaca-se em negrito os vetores que satisfazem o conceito de Forma Canônica
    para o modelo.
\end{frame}

\subsection{Solução Básica Viável}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Solução Básica Viável}
    Quando um PPL está na Forma Canônica, temos uma Solução Básica Viável 
    imediata. Essa é uma solução que está em um dos vértices do gráfico que
    exprime a solução gráfica para o problema.
    \newline\newline
    Portanto, do exemplo anterior, o vetor $S = \{X_7, X_2, X_4\}$ que
    identifica as variáveis que estão na base, é uma Solução Básica Viável do
    problema.
\end{frame}

\begin{frame}{Solução Básica Viável}
    Seguindo o que foi exposto no slide anterior, temos:
    \newline\newline
    \[ \left( \begin{array}{c}
X_1\\
X_2\\
X_3\\
X_4\\
X_5\\
X_6\\
X_7 \end{array} \right) =
\left( \begin{array}{c}
0\\
8\\
0\\
2\\
0\\
0\\
1 \end{array} \right)\]
\newline\newline
Ou seja: $X_1 = 0; X_2 = 8; X_3 = 0; X_4 = 2; X_5 = 0; X_6 = 0; X_7 = 1$
\end{frame}

\section{Método Simplex}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Método Simplex}
 Nesta seção finalmente estudaremos um dos principais métodos de resolução de
 PPL -- o Método Simplex.
 \newline\newline
 Será estudado nesse primeiro momento o algoritmo fundamental, e em seguida
 veremos casos especiais e as operações necessárias para tratá-los.
\end{frame}

\begin{frame}{Método Simplex}
 O Método Simplex é um algoritmo que nos permitirá caminhar pelos vértices do
 gráfico das restrições do PPL.
 \newline\newline
 Cada vértice é nada mais nada menos que uma Solução Básica Viável do PPL.
 Navegando por estas soluções, encontraremos aquela (ou aquelas -- se existerem)
 que é a solução ótima do PPL.
\end{frame}

\begin{frame}{Método Simplex}
 O Método Simplex segue as seguintes etapas:
 \begin{enumerate}
  \item Encontrar a primeira Solução Básica Viável;
  \item Verificar se existem vértices adjacentes viáveis que melhoram a Função
  Objetivo (teste de otimalidade);
  \item Caso sim:
  \begin{enumerate}
   \item Selecionar que variável entrará na base;
   \item Selecionar que variável sairá da base;
   \item Pivotear a matriz, trazendo a substituindo a variável que sairá da
   base pela que irá entrar;
   \item Voltar para verificação dos vértices adjacentes;
  \end{enumerate}
  \item Caso não:
  \begin{enumerate}
   \item Solução ótima encontrada, encerrar o algoritmo.
  \end{enumerate}
 \end{enumerate}

\end{frame}

\subsection{Tableau e Execução do Simplex}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Tableau Simplex}
 O Tableau Simplex é uma forma de esquematizar o PPL a fim de ser aplicado o
 Método Simplex. Tomamos como exemplo o PPL abaixo em sua Forma Canônica:
 \newline\newline
    \begin{center}
    $\text{Min Z} = -20X_1 - 24X_2$
    \newline Suj. a: $\begin{cases}
    3X_1 + 6X_2 + X_3 = 60\\
    4X_1 + 2X_2 + X_4 = 32\\
    X_i \geq 0, \forall i = 1, 2, 3, 4
    \end{cases}$
    \end{center}
\end{frame}

\begin{frame}{Tableau Simplex}
    Abaixo temos o PPL anterior disposto sobre o Tableau Simplex:
    \newline\newline
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & 3 & 6 & 1 & 0 & 60 \\
$X_4$ & 4 & 2 & 0 & 1 & 32 \\ \hline
F. O. & -20 & -24 & 0 & 0 & Q(X)
    \end{tabular}
    \end{table}
    Nesse caso, tem-se os coeficientes das variáveis nas Restrições e na Função
    Objetivo dispostos no tableau seguindo as ordens das variáveis na primeira
    linha. Nesse momento temos a solução $X = \{0, 0, 60, 32\}$.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $\mathbf{X_1}$ & $\mathbf{X_2}$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & 3 & 6 & 1 & 0 & 60 \\
$X_4$ & 4 & 2 & 0 & 1 & 32 \\ \hline
F. O. & \textbf{-20} & \textbf{-24} & 0 & 0 & Q(X)
    \end{tabular}
    \end{table}
    
    Com a primeira solução já encontrada, passa-se para a etapa de avaliação se
    há outras variáveis que, caso entrem na base, melhorem a Função Objetivo.
    \newline\newline
    Olhando a linha da Função Objetivo, vemos que há os valores \textbf{-20} e
    \textbf{-24}. Isso significa que a entrada dessas variáveis reduzirá a
    Função Objetivo -- o que é bom para o nosso problema de Minimização. Devemos
    decidir qual dessas variáveis entrará para a base.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $\mathbf{X_2}$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & 3 & 6 & 1 & 0 & 60 \\
$X_4$ & 4 & 2 & 0 & 1 & 32 \\ \hline
F. O. & -20 & \textbf{-24} & 0 & 0 & Q(X)
    \end{tabular}
    \end{table}
    
    Com o valor de \textbf{-24} a variável $X_2$ mostra que, caso entre na
    base, reduzirá a Função Objetivo em um valor maior que a candidata $X_1$.
    Portanto, ela entrará na base.
    \newline\newline
    Para encontrar a variável que sairá da base faz-se o teste do quociente de
    menor valor: divide-se os valores de $b_j$ pelos coeficientes da coluna da
    variável que entrará (no caso, $\mathbf{X_2}$) e escolhe-se o de menor
    valor.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $\mathbf{X_2}$ & $X_3$ & $X_4$ & b \\ \hline
$\mathbf{X_3}$ & 3 & 6 & 1 & 0 & 60 \\
$X_4$ & 4 & 2 & 0 & 1 & 32 \\ \hline
F. O. & -20 & \textbf{-24} & 0 & 0 & Q(X)
    \end{tabular}
    \end{table}
    
    No exemplo, teremos o teste dividindo \textbf{60 / 6} e \textbf{32 / 2}, que
    resultarão respectivamente em 10 e 16 -- portanto a variável que sairá da
    base será $\mathbf{X_3}$ (que é a variável que está na linha da operação).
    \newline\newline
    Para que $\mathbf{X_2}$ entre na base deve-se transformar sua respectiva
    coluna em uma coluna com valores de matriz Identidade. Para isso serão
    executadas transformações lineares sobre o tableau.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|ll}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b & \ \\ \hline
$X_2$ & 1/2 & 1 & 1/6 & 0 & 10 & ($L_1 :6$)\\
$X_4$ & 3 & 0 & -1/3 & 1 & 12 & ($L_1 * (- 2) + L_2$)\\ \hline
F. O. & -8 & 0 & 4 & 0 & Q(X) + 240 & ($L_1 * 24 + L_3$)
    \end{tabular}
    \end{table}

    A esse conjunto de transformações lineares chama-se pivoteamento. Primeiro,
    divide-se toda a linha do coeficiente escolhido pelo número que estará na
    intersecção entre a coluna da variável que irá entrar e a linha selecionada.
    Para o exemplo, divide-se a linha $L_1$ por \textbf{6}.
    \newline\newline
    Depois deve-se zerar os demais coeficientes da coluna $\mathbf{X_2}$. Para
    zerar a $L_2$, multiplica-se $L'_1$ por \textbf{-2} e soma-se a $L_2$. Para
    zerar a linha da Função Objetivo, multiplica-se $L'_1$ por \textbf{24} e 
    soma-se a $L_3$.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $\mathbf{X_1}$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_2$ & 1/2 & 1 & 1/6 & 0 & 10 \\
$\mathbf{X_4}$ & 3 & 0 & -1/3 & 1 & 12 \\ \hline
F. O. & \textbf{-8} & 0 & 4 & 0 & Q(X) + 240
    \end{tabular}
    \end{table}

    Tem-se agora uma nova solução, $\{X_1, X_2, X_3, X_4\} = \{0, 10, 0, 12\}$.
    Percebe-se pela linha da Função Objetivo que, caso $X_1$ entre na base, a
    Função Objetivo será reduzida.
    \newline\newline
    Portanto, $X_1$ é a próxima variável a entrar na base. Fazendo o teste do
    quociente, descobre-se que $X_4$ é a variável que sairá da base.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|ll}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b & \ \\ \hline
$X_2$ & 0 & 1 & 2/9 & -1/6 & 8 & ($L_2 * 1/2 + L_1$)\\
$X_1$ & 1 & 0 & -1/9 & 1/3 & 4 &  ($L_2 : 3$)\\ \hline
F. O. & 0 & 0 & 28/9 & 8/3 & Q(X) + 272 & ($L_2 * 8 + L_3$)
    \end{tabular}
    \end{table}

    $X_1$ entra na base e executamos o pivoteamento do tableau. Divide-se $L_2$
    por 3 para termos o valor 1 na intersecção da coluna $X_1$ e $L_2$, e em
    seguida realizamos as operações descritas na tabela para zerar as demais
    variáveis da coluna $X_1$.
    \newline\newline
    Percebe-se que na linha da Função Objetivo não há mais qualquer variável que
    reduza a função caso entre na base. Portanto, encontramos a solução ótima
    do problema.
\end{frame}

\begin{frame}{Execução do Simplex}
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_2$ & 0 & 1 & 2/9 & -1/6 & 8 \\
$X_1$ & 1 & 0 & -1/9 & 1/3 & 4 \\ \hline
F. O. & 0 & 0 & 28/9 & 8/3 & Q(X) + 272
    \end{tabular}
    \end{table}

    A Solução Ótima portanto é $\{X_1, X_2, X_3, X_4\} = \{4, 8, 0, 0\}$, e o
    valor respectivo da Função Objetivo é 272, pois:

    \begin{center}
        Q(X) + 272 = 0 \\ Q(X) = -272\\
    \end{center}
    
    E:
    
    \begin{center}
        Max Q(X) = - Min (-Q(X))\\Max Q(X) = - (-272)\\Max Q(X) = 272\\
    \end{center}

\end{frame}

\subsection{Exemplo -- Modelagem e Resolução}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Exemplo}
    Uma cooperativa de ribeirinhos decidiu investir no cultivo de açaí e
    cupuaçu em suas terras. Calculou-se que cada m$^2$ de açaí custaria R\$ 2 em
    manutenção, enquanto o m$^2$ de cupuaçú custa R\$ 3. O total de recursos
    destinado a manutenção da roça é da monta de R\$ 200. Cada m$^2$ de açaí
    exige um volume de 4m$^3$ de água, enquanto o cupuaçú necessita de 3m$^3$. O
    volume total de água disponível para essa cultura é de 240m$^3$. Na venda,
    cada m$^2$ de açaí produzirá R\$ 20 de lucro, enquanto o valor produzido por
    m$^2$ de cupuaçú será de R\$ 25. Quantos m$^2$ de cada cultura deverão ser
    cultivados pela cooperativa de forma a conseguir o lucro máximo nas vendas?
\end{frame}

\begin{frame}{Exemplo}
    Modelagem do Problema:
    \newline
    \begin{center}
    $\text{Max Z} = 20X_1 + 25X_2$
    \newline Suj. a: $\begin{cases}
    2X_1 + 3X_2 \leq 120\\
    4X_1 + 3X_2 \leq 240\\
    X_i \geq 0, \forall i = 1, 2
    \end{cases}$
    \end{center}
    Onde:
    \begin{itemize}
     \item $X_1$ é a área em m$^2$ destinada ao açaí;
     \item $X_2$ é a área em m$^2$ destinada ao cupuaçú.
    \end{itemize}
\end{frame}

\begin{frame}{Exemplo}
    Problema na Forma Padrão:
    \newline
    \begin{center}
    $\text{Min Z} = -20X_1 - 25X_2$
    \newline Suj. a: $\begin{cases}
    2X_1 + 3X_2 + X_3 = 120\\
    4X_1 + 3X_2 + X_4 = 240\\
    X_i \geq 0, \forall i = 1, 2, 3, 4
    \end{cases}$
    \end{center}
\end{frame}

\begin{frame}{Exemplo}
    Tableau inicial:
    \newline
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & 2 & 3 & 1 & 0 & 200 \\
$X_4$ & 4 & 3 & 0 & 1 & 240 \\ \hline
F. O. & -20 & -25 & 0 & 0 & Z
    \end{tabular}
    \end{table}

    \begin{itemize}
     \item Entra $X_2$ (menor coeficiente negativo na linha da Função Objetivo);
     \item Sai $X_3$ (menor resultado das divisões no teste do quociente).
    \end{itemize}
\end{frame}

\begin{frame}{Exemplo}
    Iteração 1:
    \newline
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_2$ & 2/3 & 1 & 1/3 & 0 & 200/3 \\
$X_4$ & 2 & 0 & -1 & 1 & 40 \\ \hline
F. O. & -10/3 & 0 & 25/3 & 0 & Z + 5000/3
    \end{tabular}
    \end{table}

    \begin{itemize}
     \item Entra $X_1$ (menor coeficiente negativo na linha da Função Objetivo);
     \item Sai $X_4$ (menor resultado das divisões no teste do quociente).
    \end{itemize}
\end{frame}

\begin{frame}{Exemplo}
    Iteração 2:
    \newline
    \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_2$ & 0 & 1 & 2/3 & -1/3 & 160/3 \\
$X_1$ & 1 & 0 & -1/2 & 1/2 & 20 \\ \hline
F. O. & 0 & 0 & 20/3 & 5/3 & Z + 5200/3
    \end{tabular}
    \end{table}

    Solução Ótima encontrada!\\
    \{$X_1$, $X_2$, $X_3$, $X_4$\} = \{20, 160/3, 0, 0\}\\
    Z* = 5200/3
\end{frame}

\section{Situações Especiais}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Situações Especiais}
 Nos exemplos, vimos casos onde o Simplex encontrou uma Solução Ótima para os
 problemas em resolução.
 \newline\newline
 Entretanto, existem alguns casos especiais dignos de nota. Esses casos são
 passíveis de identificação pelo Método Simplex. São eles:
 \begin{itemize}
  \item Problema Ilimitado;
  \item Problema com Múltiplas (Infinitas) Soluções Ótimas.
 \end{itemize}
\end{frame}

\subsection{Problema Ilimitado}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Problema Ilimitado}
 O Problema Ilimitado ocorre quando dada Variável de Decisão pode ser melhorada
 mas não há restrição que a limite.
 \newline
 Vejamos o seguinte problema:
 \begin{center}
 $\text{Min Z} = -X_1 - 3X_2$
 \newline Suj. a: $\begin{cases}
 X_1 - 2 X_2 \leq 4\\
 -X_1 + X_2 \leq 3\\
 X_i \geq 0, \forall i = 1, 2
 \end{cases}$
\end{center}
\end{frame}

\begin{frame}{Problema Ilimitado}
 O \textit{tableau} inicial da forma padrão do problema seria:
 \newline
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & 1 & -2 & 1 & 0 & 4 \\
$X_4$ & -1 & 1 & 0 & 1 & 3 \\ \hline
F. O. & -1 & -3 & 0 & 0 & Z
    \end{tabular}
   \end{table}
   Já o \textit{tableau} final terá a seguinte configuração:
 \newline
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & -1 & 0 & 1 & 2 & 10 \\
$X_2$ & -1 & 1 & 0 & 1 & 3 \\ \hline
F. O. & -4 & 0 & 0 & 3 & Z
    \end{tabular}
   \end{table}
\end{frame}

\begin{frame}{Problema Ilimitado}
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & -1 & 0 & 1 & 2 & 10 \\
$X_2$ & -1 & 1 & 0 & 1 & 3 \\ \hline
F. O. & -4 & 0 & 0 & 3 & Z
    \end{tabular}
   \end{table}
   \ \newline\newline
   Perceba que no \textit{tableau} final ainda haveria espaço para otimizar a
   Função Objetivo ($X_1$ é candidata a entrar na base), porém não há qualquer
   variável candidata a sair da base (o teste do quociente não é possível de
   ser realizado para $X_1$).
\end{frame}

\begin{frame}{Problema Ilimitado}
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & -1 & 0 & 1 & 2 & 10 \\
$X_2$ & -1 & 1 & 0 & 1 & 3 \\ \hline
F. O. & -4 & 0 & 0 & 3 & Z
    \end{tabular}
   \end{table}
   \ \newline\newline
   Isso ocorre porque não há qualquer restrição no modelo que limite o
   crescimento de $X_1$. Portanto, nesses casos, diz-se que temos um Problema
   Ilimitado.
\end{frame}

\subsection{Problema com Múltiplas (Infinitas) Soluções Ótimas}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Problema com Infinitas Soluções Ótimas}
 É possível que um determinado problema tenha múltiplas soluções ótimas. Nesse
 caso as múltiplas soluções serão infinitas, pois são resultantes da combinação
 linear entre as Variáveis de Decisão.
 \newline
 Vejamos o seguinte problema:
 \begin{center}
 $\text{Min Z} = -2X_1 - 4X_2$
 \newline Suj. a: $\begin{cases}
 X_1 + 2 X_2 \leq 4\\
 -X_1 + X_2 \leq 1\\
 X_i \geq 0, \forall i = 1, 2
 \end{cases}$
\end{center}
\end{frame}

\begin{frame}{Problema com Infinitas Soluções Ótimas}
 O \textit{tableau} inicial da forma padrão do problema seria:
 \newline
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_3$ & 1 & 2 & 1 & 0 & 4 \\
$X_4$ & -1 & 1 & 0 & 1 & 1 \\ \hline
F. O. & -2 & -4 & 0 & 0 & Z
    \end{tabular}
   \end{table}
   Já o \textit{tableau} final terá a seguinte configuração:
 \newline
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_1$ & 1 & 0 & 1/3 & -2/3 & 2/3 \\
$X_2$ & 0 & 1 & 1/3 & 1/3 & 5/3 \\ \hline
F. O. & 0 & 0 & 2 & 0 & Z + 8
    \end{tabular}
   \end{table}
\end{frame}

\begin{frame}{Problema com Infinitas Soluções Ótimas}
 \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_1$ & 1 & 0 & 1/3 & -2/3 & 2/3 \\
$X_2$ & 0 & 1 & 1/3 & 1/3 & 5/3 \\ \hline
F. O. & 0 & 0 & 2 & 0 & Z + 8
    \end{tabular}
   \end{table}
    \ \newline
   Na última linha dos coeficientes da Função Objetivo, é possível ver que há
   um ``0'' a mais que as colunas da matriz identidade. Esse é o coeficiente da
   variável $X_4$. Colocando-a na base teremos:
   \begin{table}[]
    \centering
    \begin{tabular}{l|llll|l}
\ & $X_1$ & $X_2$ & $X_3$ & $X_4$ & b \\ \hline
$X_1$ & 1 & 2 & 1 & 0 & 4 \\
$X_4$ & 0 & 3 & 1 & 1 & 5 \\ \hline
F. O. & 0 & 0 & 2 & 0 & Z + 8
    \end{tabular}
   \end{table}
\end{frame}

\begin{frame}{Problema com Infinitas Soluções Ótimas}
 No primeiro \textit{tableau} temos a solução $X = \{2/3, 5/3, 0, 0\}$, e no
 segundo temos $X = \{4, 0, 0, 5\}$. Em ambos o resultado da Função Objetivo é
 -8.
 \ \newline\newline
 Na verdade, qualquer combinação linear entre essas duas soluções será solução
 ótima para o problema. Portanto, representamos a solução ótima por:
 \begin{center} 
 X* = $\alpha \left[ \begin{array}{c}2/3\\5/3\\0\\0\end{array} \right] + (1 -
 \alpha) \left[ \begin{array}{c}4\\0\\0\\5\end{array} \right]$
 \end{center}
 Com $\alpha$ variando entre 0 e 1.
\end{frame}

\section{Conclusões}
\begin{frame}{Conteúdo}
	\tableofcontents[currentsection,currentsubsections]
\end{frame}

\begin{frame}{Conclusões}
 \begin{itemize}
  \item Simplex é o principal método de resolução de problemas de programação
  linear;
  \item Para resolver um problema desse tipo, primeiro deveremos colocá-lo na
  forma padrão, partir de uma solução básical viável, e repetir o algoritmo
  simplex até chegar na solução ótima.
  \item Simplex também permite identificar casos especiais de resolução, quando
  o problema é ilimitado ou quando tem infinitas soluções.
 \end{itemize}
\end{frame}

\begin{frame}
    \maketitle
\end{frame}

\end{document}
