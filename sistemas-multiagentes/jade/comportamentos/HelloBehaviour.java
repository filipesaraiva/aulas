import jade.core.behaviours.Behaviour;

public class HelloBehaviour extends Behaviour {

  public void action() {
    System.out.println("Hello Behaviour!")
  }

  public boolean done() {
    return true;
  }
}
